import { useState, useEffect } from "react";
import { useRouter } from 'next/router'
import Link from 'next/link';
import BlogShareIcons from "../../components/common/BlogShareIcons";
import BlogGallery from "../../components/sections/BlogGallery";
import CoachSection from "../../components/sections/CoachSection";
import Accordian from "../../components/common/Accordian";
import Footer from "../../components/layout/Footer";
import Slider from "react-slick";
import { Modal } from "react-bootstrap";
import Head from 'next/head'
import styles from '../page.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircle } from "@fortawesome/free-solid-svg-icons";
import urlSet, { baseUrl } from "../../utils/urls";
import axios from 'axios';
import BlogCard from "../../components/cards/BlogCard";
import SimpleLoader from '../../components/loaders/Simple'

const Blog = ({ blog }) => {
  const router = useRouter();
  const params = router.query;
  const limit = 6;
  const postUrl = "https://alanvidyushbaptist.com" + router.asPath;
  const [selectedImage, setSelectedImage] = useState(null)
  const [show, setShow] = useState(false)
  const [page, setPage] = useState(0);
  const [blogs, setBlogs] = useState([]);
  const [loading, setLoading] = useState(false);

  if (typeof window !== 'undefined') {
    const imgs = document.querySelectorAll('#blogContent img')
    imgs.forEach(img => {
      const newDiv = document.createElement('div');
      newDiv.className = 'text-center mt-5';
      newDiv.appendChild(img.cloneNode());
      console.log(newDiv);
      img.parentElement.appendChild(newDiv);
      img.parentElement.removeChild(img);
    })
  }

  useEffect(() => {
    const fetchBlogs = async () => {
      try {
        let url = urlSet.get_blogApi.url + "?limit=7";
        console.log(url);
        setLoading(true);
        const res = await axios.get(encodeURI(url));
        const resBlogs = res.data.filter((b) => {
          if (b._id === blog._id) {
            return false;
          } else {
            return true;
          }
        })
        setBlogs(resBlogs.slice(0, 6));
        setPage(1);
        setLoading(false);
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    };

    fetchBlogs();
  }, [blog._id]);

  const getNextBlogs = async () => {
    try {
      let url =
        urlSet.get_blogApi.url + "?index=" + page * limit + "&limit=" + limit;
      setLoading(true);
      const res = await axios.get(encodeURI(url));
      if (page === 0) {
        setBlogs(res.data);
      } else {
        const resBlogs = res.data.filter((b) => {
          if (b._id === blog._id) {
            return false;
          } else {
            return true;
          }
        })
        setBlogs([...blogs, ...resBlogs]);
      }

      setPage(page + 1);
      setLoading(false);
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };

  const enlargeImage = (index) => {
    setSelectedImage(index)
    setShow(true)
  }

  const resizeHandler = () => {
    const navbar = document.getElementById("navbar");
    const contents = document.getElementById("stickyContents");
    if (!navbar) return;
    if (!contents) return;

    if (window.innerWidth <= 600) {
      if (
        contents.parentElement.parentElement.getBoundingClientRect().top + 50 <
        0
      ) {
        navbar.style.position = 'inherit';
        contents.classList.add("sticky2");
      } else {
        navbar.style.position = 'fixed';
        contents.classList.remove("sticky2");
      }
    } else {
      navbar.style.position = 'fixed';
      contents.classList.add("sticky3");
      contents.classList.remove("sticky2");
    }
  };

  useEffect(() => {
    const setProgress = () => {
      let scrollPercentRounded = Math.round(
        (window.scrollY / (document.body.scrollHeight - window.innerHeight)) *
        100
      );
      document.getElementById("blogContentProgress").style.width =
        scrollPercentRounded + "%";
    };

    window.addEventListener("scroll", setProgress);

    return () => {
      window.removeEventListener("scroll", setProgress);
      if (document.getElementById("blogContentProgress")) {
        document.getElementById("blogContentProgress").style.width = "0%";
      }
      if (!document.getElementById("navbar").classList.contains("fixed-top")) {
        document.getElementById("navbar").classList.add("fixed-top");
      }
    };
  }, [params]);

  useEffect(() => {
    resizeHandler();
    window.addEventListener("scroll", resizeHandler);
    return () => {
      window.removeEventListener("scroll", resizeHandler);
    };
  }, []);

  const routeToSection = (id) => {
    document.getElementById(id).scrollIntoView();
  };

  const settings = {
    autoplay: true,
    infinite: true,
    arrows: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1
  };

  const imageName = blog.headerImage.length > 0 ? blog.headerImage[0].image.split('/')[blog.headerImage[0].image.split('/').length - 1] : '';
  const tempThumbnailName = decodeURI(imageName.split('.')[0] + "_thumbnail.jpg");
  const thumbnailName = tempThumbnailName.replace(/ /g, '_');
  return (
    <div className="mb-5">
      <Head>
        <meta name="title" content={blog.title} />
        <meta name="description" content={blog.summary} />
        <meta property="og:image" content={"https://assets.alanvidyushbaptist.com/thumbnails/" + thumbnailName} />
        <meta name="content-type" content="blog" />
        <meta property="og:image:width" content="800" />
        <meta property="og:image:height" content="391" />
        <meta property="og:image:alt" content={blog.title} />
        <meta name="author" content={blog.author} />

        <title>{blog.title}</title>
        <meta property="og:title" content={blog.title} />
        <meta property="og:description" content={blog.summary} />
        <meta property="og:url" content={postUrl} />
        <meta property="og:site_name" content="ALAN VIDYUSH BAPTIST" />
        <meta name="twitter:title" content={blog.title} />
        <meta name="twitter:description" content={blog.summary} />
        <meta name="twitter:image" content={"https://assets.alanvidyushbaptist.com/thumbnails/" + thumbnailName} />
      </Head>
      <div id="blogContentProgress" style={{
        height: "5px",
        position: "fixed",
        top: "0px",
        width: "0%",
        background: "yellow",
        zIndex: "999999",
      }}></div>


      <div style={{ flex: 1 }} className="mb-5 dynamicPageSection">
        {blog && (
          <section className="blogAvailable">
            <div className="blogImageContainer mt-1 mt-md-4">

              <Slider {...settings}>
                {blog &&
                  blog.headerImage.map((elem, index) => {
                    return (
                      <div
                        className={`carousel-item ${index === 0 ? 'active' : ''}`}
                        data-interval={blog["data_interval"]}
                        key={index}
                      >
                        <img
                          src={elem["image"]}
                          alt={elem["title"]}
                          width="1100"
                          height="500"
                        />
                      </div>
                    )
                  })}
              </Slider>
            </div>
            <div className="blogWriter">
              <div id="blogWriter">
                {blog && blog.client && (
                  <div dangerouslySetInnerHTML={{ __html: blog.client }} />
                )}
              </div>

              <CoachSection coaches={blog.coach} />

              <div className={`text-center ${styles.fw700} ${styles.mfo12} mt-5`} id="brands">
                {blog && blog.brands && `BRANDS: ${blog.brands}`}
              </div>
            </div>
            <div className="blogHeading text-center mt-5" id="blogHeading">
              {blog && `${blog.title}`}
            </div>
            <hr className="blogBorder" />
            <div className={`${styles.fo13} mt-5 text-center blogDate`} id="blogdate">
              {blog && `${blog.date}`}
            </div>
            <div className="blogContentContainer">
              <div className="row m-0 flex-sm-row-reverse">
                <div className="col-sm-11 p-0">
                  <div className="row m-0 flex-sm-row-reverse">
                    <div className="col-sm-4 blogContents p-0 mt-4 mt-sm-0">
                      <div className="stickyContents sticky3" id="stickyContents">
                        <Accordian title="Contents">
                          <div className="pt-2">
                            <ul className="ps-2" id="contentList2">
                              {blog &&
                                blog.content.map((elem, index) => {
                                  return (
                                    <li className={`${styles.fo16} ${styles.cursorPointer}`} onClick={() => routeToSection(elem.id)} key={index}>
                                      <FontAwesomeIcon icon={faCircle} className={`${styles.fo7} me-3 ${styles.bco} ${styles.fw600}`} />
                                      {elem["title"]}
                                    </li>
                                  );
                                })}
                            </ul>
                          </div>
                        </Accordian>
                      </div>
                    </div>

                    <div className="col-sm-8 mt-5 mt-sm-0">
                      <div id="blogContent">
                        {blog &&
                          blog.body.map((elem, index) => {
                            return (
                              <div key={index}>
                                <div className={`blogContent ${styles.fw600} ${styles.fo30} mb-2 mt-5`} id={"topic" + elem["id"]}> {elem["heading"]}</div>
                                <div className={`blogContent ${styles.fo19} ${styles.mfo15}`} dangerouslySetInnerHTML={{ __html: elem["paragraph"], }}></div>
                              </div>
                            );
                          })}
                      </div>
                    </div>
                    <Modal show={blog && show} onHide={() => setShow(false)} animation={true} size="lg" backdrop={true} onExited={() => setSelectedImage(false)} centered>
                      <Modal.Body as="div" bsPrefix="none">
                        <button type="button" className="close galleryClose" data-dismiss="modal" onClick={() => setShow(false)}>&times;</button>
                        {blog && (
                          <img
                            src={blog.gallery[selectedImage || 0].image}
                            height="300"
                            className="w-100 h-100 copyright_img"
                            alt={"image_" + blog.gallery[selectedImage || 0].title}
                          />
                        )}
                      </Modal.Body>
                    </Modal>
                    <BlogGallery>
                      {blog &&
                        blog.gallery.map((image, index) => {
                          const clickHandler = (event) => {
                            if (event.detail >= 2) enlargeImage(index)
                          }
                          return (
                            <div key={index} className="p-4" onClick={clickHandler}>
                              <img
                                src={image["image"]}
                                height="300"
                                className="w-100 copyright_img no-outline"
                                alt={"image_" + image["title"]}
                              />
                            </div>
                          );
                        })}
                    </BlogGallery>
                  </div>
                </div>
                <BlogShareIcons postTitle={blog.title} postUrl={postUrl} postSummary={blog.summary} />
              </div>
            </div>
          </section>
        )}

        {!blog && (<section id="blogNotAvailable">
          <div className="text-center fw-600 fo-24 mb-4 mt-5">
            Sorry, this page isn&apos;t available.
          </div>
          <div className="text-center">
            The link you followed may be broken, or the page may have been
            removed.{" "}
            <Link href="/" className="bco fw-600">
              Go back to Home.
            </Link>
          </div>
        </section>)}
      </div>


      <div className={`${styles.textDark} ${styles.fo30} ${styles.mfo22} p-3 mt-4 mt-sm-0 p-sm-5 text-center ${styles.fw700}`}>
        MORE FROM ALAN BAPTIST
      </div>
      <div className="row m-0 mt-3" id="displayAllBlogs">
        {blogs.map((blog, index) => (
          <BlogCard blog={blog} key={index} />
        ))}
      </div>
      {loading && <SimpleLoader />}
      {!loading && blogs && blogs.length > 0 && (
        <>
          <div className="text-center mt-5 homeBottom" id="readmorebutton">
            {limit * page <= blogs.length ? (
              <button
                className="btn website-button bg-dark text-white"
                onClick={getNextBlogs}
              >
                READ MORE
              </button>
            ) : (
              <div className="fo-20 fw-600 text-center">
                <p>Revisit us again for more!</p>
              </div>
            )}
          </div>
        </>
      )}

      {!loading && blogs && blogs.length <= 0 && (
        <h2 className="text-center">No Blogs Found</h2>
      )}

      <div className="mt-5 mb-5" style={{ height: '20px' }}></div>

      <Footer />
    </div>
  );
};

export async function getStaticPaths() {
  const res = await fetch(`${baseUrl}/api/v1/blogs?minimal=true`);
  const blogs = await res.json();
  const paths = blogs.map((blog) => {
    return {
      params: {
        id: blog.slug,
      },
    };
  });
  return {
    paths: paths,
    fallback: 'blocking', // can also be true or 'blocking'
  };
}

// `getStaticPaths` requires using `getStaticProps`
export async function getStaticProps(context) {
  const res = await fetch(`${baseUrl}/api/v1/blogs/${context.params.id}`);
  const blog = await res.json();
  console.log(context.params.id, blog.slug);
  return {
    props: { blog },
    revalidate: 60,
  };
}

export default Blog;

import axios from 'axios';
import { useEffect, useState } from 'react';
import styles from './page.module.css';
import urlSet from '../utils/urls';
import BlogCard from '../components/cards/BlogCard';
import Footer from '../components/layout/Footer';
import Link from 'next/link';
import Head from 'next/head';

const Partners = () => {
  const limit = 6;
  const startIndex = 0;
  const [page, setPage] = useState(0);
  const [blogs, setBlogs] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    const fetchBlogs = async () => {
      try {
        let url = urlSet.get_blogApi.url + "?limit=6";

        setLoading(true);
        const res = await axios.get(encodeURI(url));
        setBlogs(res.data);
        setPage(1);
        setLoading(false);
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    };

    fetchBlogs();
  }, []);

  const getNextBlogs = async () => {
    try {
      let url =
        urlSet.get_blogApi.url + "?index=" + page * limit + "&limit=" + limit;
      setLoading(true);
      const res = await axios.get(encodeURI(url));
      if (page === 0) {
        setBlogs(res.data);
      } else {
        setBlogs([...blogs, ...res.data]);
      }

      setPage(page + 1);
      setLoading(false);
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };
  return(
    <div className='mt-0'>
            <Head>
        <title>Partners - Alan Vidyush Baptist</title>
      </Head>
      <section>
        <div className="text-center">
          <img loading="lazy" src='/assets/banners/partners_banner.png' className="w-100" alt="PartnersBanner"></img>
        </div>
      </section>

      <section className={`${styles.partnersSection} mt-4`}>
        <div className="row m-0">
          <div className={`col-sm-8 p-0 ${styles.bgGrey}`}>
            <div className="row m-0 ps-sm-5 pe-sm-5 h-100">
              <div className="col-5 col-sm-5 m-auto text-center">
                <img loading="lazy" src='/assets/partners/partner_1.png' className={`${styles.w70} ${styles.mow100}`} alt="Partner1"></img>
              </div>
              <div className="col-1 col-sm-2 pt-5 pb-5 p-sm-5 text-center m-auto">
                <div className={styles.midBorder}></div>
              </div>
              <div className="col-6 col-sm-5 ps-0 pt-sm-5 pe-sm-0 m-auto">
                <div className={`${styles.fo28} ${styles.mfo25} ${styles.fw800} text-center`}>
                  Neera Katwal
                </div>
                <div className={`${styles.fo15} ${styles.mfo20} ${styles.fw800} text-center`}>
                  <Link href="/blogs?partner=Neera_Katwal">
                    <button className={`btn ${styles.partnersButton} mt-3`}>
                      READ MORE
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className={`col-sm-4 p-1 pt-4 pb-4 p-sm-5 text-center ${styles.bgDark} d-flex`}>
            <div className="m-auto">
              <Link href="/blogs?partner=Neera">
                <div className={`${styles.fo18} text-white ${styles.fw600} ${styles.mfo14}`}>
                  Marathon Running Coach, Certified Yoga Instructor, Strength
                  Coach, Fitness Model.
                </div>
              </Link>
              <hr></hr>
            </div>
          </div>
        </div>

        <div className="row m-0 d-flex flex-row-reverse">
          <div className={`col-sm-8 p-0 ${styles.bgGrey}`}>
            <div className="row m-0 ps-sm-5 pe-sm-5 h-100">
              <div className="col-5 col-sm-5 m-auto text-center">
                <img loading="lazy" src='/assets/partners/partner_2.png' className={`${styles.w70} ${styles.mow100}`} alt="Partner2"></img>
              </div>
              <div className="col-1 col-sm-2 pt-5 pb-5 p-sm-5 text-center m-auto">
                <div className={styles.midBorder}></div>
              </div>
              <div className="col-6 col-sm-5 ps-0 pt-sm-5 pe-sm-0 m-auto">
                <div className={`${styles.fo28} ${styles.mfo25} ${styles.fw800} text-center`}>
                  Vivek Baptist
                </div>
                <div className={`${styles.fo15} ${styles.mfo20} ${styles.fw800} text-center`}>
                  <Link href="/blogs?partner=Vivek_Baptist">
                    <button className={`btn ${styles.partnersButton} mt-3`}>
                      READ MORE
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className={`col-sm-4 p-2 pt-4 pb-4 p-sm-5 text-center ${styles.bgDark} d-flex`}>
            <div className="m-auto">
              <Link href="/blogs?partner=Vivek Baptist">
                <div className={`${styles.fo18} text-white ${styles.fw600} ${styles.mfo14}`}>
                  Pain Science Expert Consultant, Powerlifting Athlete, Gym
                  Owner, Columnist
                </div>
              </Link>
              <hr></hr>
            </div>
          </div>
        </div>


        <div className="row m-0">
          <div className={`col-sm-8 p-0 ${styles.bgGrey}`}>
            <div className="row m-0 ps-sm-5 pe-sm-5 h-100">
              <div className="col-5 col-sm-5 m-auto text-center">
                <img loading="lazy" src='/assets/partners/partner_3.png' className={`${styles.w70} ${styles.mow100}`} alt='Partner3'></img>
              </div>
              <div className="col-1 col-sm-2 pt-5 pb-5 p-sm-5 text-center m-auto">
                <div className={styles.midBorder}></div>
              </div>
              <div className="col-6 col-sm-5 ps-0 pt-sm-5 pe-sm-0 m-auto">
                <div className={`${styles.fo28} ${styles.mfo25} ${styles.fw800} text-center`}>
                  Vinit Baptist
                </div>
                <div className={`${styles.fo15} ${styles.mfo20} ${styles.fw800} text-center`}>
                  <Link href="/blogs?partner=Vinit_Baptist">
                    <button className={`btn ${styles.partnersButton} mt-3`}>
                      READ MORE
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className={`col-sm-4 p-1 pt-4 pb-4 p-sm-5 text-center ${styles.bgDark} d-flex`}>
            <div className="m-auto">
              <Link href="/blogs?partner=Vinit Baptist">
                <div className={`${styles.fo18} text-white ${styles.fw600} ${styles.mfo14}`}>
                  Fitness Expert, Strength & Conditioning Coach, Athlete,
                  Mega-Gym Owner
                </div>
              </Link>
              <hr></hr>
            </div>
          </div>
        </div>

        <div className="row m-0 d-flex flex-row-reverse">
          <div className={`col-sm-8 p-0 ${styles.bgGrey}`}>
            <div className="row m-0 ps-sm-5 pe-sm-5 h-100">
              <div className="col-5 col-sm-5 m-auto text-center">
                <img loading="lazy" src='/assets/partners/partner_4.png' className={`${styles.w70} ${styles.mow100}`} alt='Partner4'></img>
              </div>
              <div className="col-1 col-sm-2 pt-5 pb-5 p-sm-5 text-center m-auto">
                <div className={styles.midBorder}></div>
              </div>
              <div className="col-6 col-sm-5 ps-0 pt-sm-5 pe-sm-0 m-auto">
                <div className={`${styles.fo28} ${styles.mfo25} ${styles.fw800} text-center`}>
                  Minhaj Akhtar Khan
                </div>
                <div className={`${styles.fo15} ${styles.mfo20} ${styles.fw800} text-center`}>
                  <Link href="/blogs?partner=Minhaj_Akhtar_Khan">
                    <button className={`btn ${styles.partnersButton} mt-3`}>
                      READ MORE
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className={`col-sm-4 p-2 pt-4 pb-4 p-sm-5 text-center ${styles.bgDark} d-flex`}>
            <div className="m-auto">
              <Link href="/blogs?partner=Minhaj Akhtar Khan">
                <div className={`${styles.fo18} text-white ${styles.fw600} ${styles.mfo14}`}>
                  CFO, Financial Consultant, Tax Advisor, Qualified Accountant
                </div>
              </Link>
              <hr></hr>
            </div>
          </div>
        </div>

        <div className="row m-0">
          <div className={`col-sm-8 p-0 ${styles.bgGrey}`}>
            <div className="row m-0 ps-sm-5 pe-sm-5 h-100">
              <div className="col-5 col-sm-5 m-auto text-center">
                <img loading="lazy" src='/assets/partners/partner_5.png' className={`${styles.w70} ${styles.mow100}`} alt='Partner5'></img>
              </div>
              <div className="col-1 col-sm-2 pt-5 pb-5 p-sm-5 text-center m-auto">
                <div className={styles.midBorder}></div>
              </div>
              <div className="col-6 col-sm-5 ps-0 pt-sm-5 pe-sm-0 m-auto">
                <div className={`${styles.fo28} ${styles.mfo25} ${styles.fw800} text-center`}>Pankaj</div>
                <div className={`${styles.fo15} ${styles.mfo20} ${styles.fw800} text-center`}>
                  <Link href="/blogs?partner=Pankaj">
                    <button className={`btn ${styles.partnersButton} mt-3`}>
                      READ MORE
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </div>
          <div className={`col-sm-4 p-2 pt-4 pb-4 p-sm-5 text-center ${styles.bgDark} d-flex`}>
            <div className="m-auto">
              <Link href="/blogs?partner=Pankaj">
                <div className={`${styles.fo18} text-white ${styles.fw600} ${styles.mfo14}`}>
                  Director, Financial Doctor, Tax Specialist
                </div>
              </Link>
              <hr></hr>
            </div>
          </div>
        </div>

      </section>


      <div className={`${styles.textDark} ${styles.fo30} ${styles.mfo22} p-3 mt-4 mt-sm-0 p-sm-5 text-center ${styles.fw700}`}>
        THE LATEST FROM OUR PARTNERS
      </div>
      <div className="row m-0 mt-3" id="displayAllBlogs">
        {blogs.map((blog, index) => (
          <BlogCard blog={blog} key={index} />
        ))}
      </div>
      {!loading && blogs && blogs.length > 0 && (
        <>
          <div className="text-center mt-5 homeBottom" id="readmorebutton">
            {limit * page <= blogs.length ? (
              <button
                className="btn website-button bg-dark text-white"
                onClick={getNextBlogs}
              >
                READ MORE
              </button>
            ) : (
              <div className="fo-20 fw-600 text-center">
                <p>Revisit us again for more!</p>
              </div>
            )}
          </div>
        </>
      )}

      {!loading && blogs && blogs.length <= 0 && (
        <h2 className="text-center">No Blogs Found</h2>
      )}
      <Footer/>
    </div>
  )
}

export default Partners;

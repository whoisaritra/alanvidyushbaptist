import { useEffect } from 'react'
import styles from './page.module.css';
import BlogGallery from "../components/sections/BlogGallery";

import Head from 'next/head';

const images = [
  '/assets/portfolio/1.jpeg',
  '/assets/portfolio/2.jpeg',
  '/assets/portfolio/3.jpeg',
  '/assets/portfolio/4.jpeg',
  '/assets/portfolio/5.jpeg',
  '/assets/portfolio/6.jpg',
  '/assets/portfolio/7.jpg',
  '/assets/portfolio/8.jpeg',
  '/assets/portfolio/9.jpg',
  '/assets/portfolio/10.jpeg',
  '/assets/portfolio/11.jpeg',
  '/assets/portfolio/12.jpeg',
];

const Portfolio = () => {

  function rightClick(e) {
    e.preventDefault();
    if (document.getElementById("contextMenu").style.display === "block")
      hideMenu();
    else {
      var menu = document.getElementById("contextMenu")
      menu.style.display = 'block';
      menu.style.left = e.pageX + "px";
      menu.style.top = e.pageY + "px";
    }
  }
  const hideMenu = () => {
    if(typeof window != 'undefined')
      document.getElementById("contextMenu").style.display = "none"
  }
  useEffect(() => {
    document.onclick = hideMenu;
    for (let i = 0; i < document.querySelectorAll(".copyright_img").length; i++) {
      document.querySelectorAll(".copyright_img")[i].oncontextmenu = rightClick;
    }
  }, [])


  return(
    <div>
      <Head>
        <title>Portfolio - Alan Vidyush Baptist</title>
      </Head>
      <div id="contextMenu" className="context-menu" style={{ display: "none" }}>
        This photo is Copyright ©️ 2022 Alan Baptist. All rights reserved.
      </div>

      <div onContextMenu={() => null} className="d-sm-none portfolioSection_mobile">
        <BlogGallery>
          {images.map((image, index) => {
            return (
              <div className="p-2" key={index}>
                <div className="card p-0 border-0">
                  <img loading="lazy" src={image} className="w-100 copyright_img" alt='PortfolioImage'></img>
                </div>
              </div>
            );
          })}
        </BlogGallery>
      </div>

      <div className="portfolioSection_desktop d-none d-sm-flex">
        <div className="p-2">
          <div className="card p-0 border-0">
            <img loading="lazy" src='/assets/portfolio/1.jpeg' className="w-100 copyright_img" alt="Portfolio1"></img>
          </div>  
        </div> 
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/2.jpeg' className="w-100 copyright_img" alt="Portfolio2"></img>
            </div>  
        </div> 
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/3.jpeg' className="w-100 copyright_img" alt="Portfolio3"></img>
            </div>  
        </div>  
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/4.jpeg' className="w-100 copyright_img" alt="Portfolio4"></img>
            </div>  
        </div> 
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/5.jpeg' className="w-100 copyright_img" alt="Portfolio5"></img>
            </div>  
        </div>  
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/6.jpg' className="w-100 copyright_img" alt="Portfolio6"></img>
            </div>  
        </div> 
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/7.jpg' className="w-100 copyright_img" alt="Portfolio7"></img>
            </div>  
        </div> 
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/8.jpeg' className="w-100 copyright_img" alt="Portfolio8"></img>
            </div>  
        </div>  
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/9.jpg' className="w-100 copyright_img" alt="Portfolio9"></img>
            </div>  
        </div> 
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/10.jpeg' className="w-100 copyright_img" alt="Portfolio10"></img>
            </div>  
        </div>  
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/11.jpeg' className="w-100 copyright_img" alt="Portfolio11"></img>
            </div>  
        </div>  
        <div className="p-2">
            <div className="card p-0 border-0">
              <img loading="lazy" src='/assets/portfolio/12.jpeg' className="w-100 copyright_img" alt="Portfolio12"></img>
            </div>  
        </div>  
      </div>

      <div className={styles.disclaimer}><span>Disclaimer -</span> Kindly note that these pictures are representative of the physique I&apos;d maintained during my brief career as a fitness model as per the industry standards in the early 2010&apos;s and is by no means an endorsement of a particular body type as an embodiment of holistic well-being. I firmly believe that fitness and wellness as a holistic concept encapsulates much more than maintaining a specific body composition ratio</div>
    </div>
  )
}

export default Portfolio;

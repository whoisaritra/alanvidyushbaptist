import Footer from "../components/layout/Footer";
import styles from './page.module.css';

const RefundPolicy = () => {
  return (
    <div>
      <section className={styles.externalPages}>
         <div className="pe-md-5 ps-md-5">
            <div className={`${styles.fo40} ${styles.fw700} ${styles.textYellow} ${styles.mfo25} mb-4 text-center`}>CANCELLATION AND REFUND POLICY</div>
            <div className={`${styles.fo18} text-white p-4 p-md-5 ${styles.mfo14} text-justify`}>Once a consultation is already underway, we have a no-cancellation policy. Further refund requests under special circumstances are entirely under Alan Baptist’s discretion.</div>
         </div>
      </section>
      <Footer />
    </div>
  );
};
export default RefundPolicy;
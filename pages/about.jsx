import styles from './page.module.css';
import Footer from '../components/layout/Footer';
import Head from 'next/head';

const About = () => {
    return(
        <div>
      <Head>
        <title>About - Alan Vidyush Baptist</title>
      </Head>
            <div className={styles.HeaderContainer}></div>
            <section>
                <div className="p-0 pt-5 pe-sm-5 ps-sm-5 pt-sm-5">
                    <div className={`text-center ${styles.about_labels}`}>
                        <picture>
                            <source media="(min-width:465px)" srcSet="/assets/labels/1.png"></source>
                            <img loading="lazy" src="/assets/labels/1.png" alt='About1'></img>
                        </picture>
                    </div>


                    <div className={styles.aboutSection} data-aos="fade-right" data-aos-delay="100">
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.precisionnutrition.com/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about1.png' className="w-100" alt='AboutImage1'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the{" "}
                                <b>PN Level 1 Course at Precision Nutrition(Canada)</b>
                            </div>
                        </div>
                        <div className="row m-0 d-flex flex-row-reverse">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.mac-nutritionuni.com/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about2.png' className="w-100" alt='AboutImage2'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the{" "}
                                <b>MNU Course at Mac Nutrition Uni(United Kingdom)</b>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.nasm.org/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about3.png' className="w-100" alt='AboutImage3'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied Certified{" "}
                                <b>
                                Sports Nutrition Coach(CSNC) at the National Academy of Sports
                                Medicine(NASM)
                                </b>
                            </div>
                        </div>
                        <div className={`row m-0 d-flex flex-row-reverse ${styles.lastAbout}`}>
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.nasm.org/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about4.jpg' className="w-100" alt='AboutImage4'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied{" "}
                                <b>
                                Sports Nutritionist Specialist at the National Exercise &
                                Sports Trainers Association(NESTA)
                                </b>
                            </div>
                        </div>
                    </div>

                    <div className={`text-center ${styles.about_labels}`}>
                        <picture>
                            <source media="(min-width:465px)" srcSet="/assets/labels/2.png"></source>
                            <img loading="lazy" src="/assets/labels/2.png" alt='About2'></img>
                        </picture>
                    </div>


                    <div className={styles.aboutSection} data-aos="fade-up" data-aos-delay="100">
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.acefitness.org/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about5.png' className="w-100" alt='AboutImage5'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the <b>Personal Trainer Course</b> at the{" "}
                                <b>American Council of Exercise(ACE)</b>
                            </div>
                        </div>
                        <div className="row m-0 d-flex flex-row-reverse">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.issaonline.com/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about6.png' className="w-100" alt='AboutImage6'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the <b>Certified Personal Trainer Course</b> at the{" "}
                                <b>International Sports Sciences Association(ISSA)</b>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.strengthandconditioning.org/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about7.png' className="w-100" alt='AboutImage7'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the <b>Strength and Conditioning Coach Course</b> at the{" "}
                                <b>Australian Strength and Conditioning Association(ASCA)</b>
                            </div>
                        </div>
                        <div className="row m-0 d-flex flex-row-reverse">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.crossfit.com/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about8.png' className="w-100" alt='AboutImage8'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied <b>CROSSFIT L-1</b>, <b>CROSSFIT L-2</b> and{" "}
                                <b>CROSSFIT</b> for Kids
                            </div>
                        </div>
                        <div className={`row m-0 d-flex ${styles.lastAbout}`}>
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://functionalanatomyseminars.com/frs-system/functional-range-conditioning/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about9.png' className="w-100" alt='AboutImage9'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied <b>Functional Range Conditioning(FRC)</b> at{" "}
                                <b>Functional Range Systems(FRS)</b>
                            </div>
                        </div>
                    </div>


                    <div className={`text-center ${styles.about_labels}`}>
                        <picture>
                            <source media="(min-width:465px)" srcSet="/assets/labels/3.png"></source>
                            <img loading="lazy" src="/assets/labels/3.png" alt='About3'></img>
                        </picture>
                    </div>


                    <div className={styles.aboutSection} data-aos="fade-right" data-aos-delay="100">
                        <div className={`row m-0 d-flex flex-row-reverse ${styles.lastAbout}`}>
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.nationalcprfoundation.com/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about10.jfif' className="w-100" alt='AboutImage10'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                <b>CPR / AED / First-Aid</b> (Adult / Child / Infant / Choking)
                                Certified by the <b>National CPR Foundation(USA)</b>
                            </div>
                        </div>
                    </div>

                    <div className={`text-center ${styles.about_labels}`}>
                        <picture>
                            <source media="(min-width:465px)" srcSet="/assets/labels/4.png"></source>
                            <img loading="lazy" src="/assets/labels/4.png" alt='About2'></img>
                        </picture>
                    </div>


                    <div className={styles.aboutSection} data-aos="fade-up" data-aos-delay="100">
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.icai.org/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about11.png' className="w-100" alt='AboutImage11'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the <b>Chartered Accountancy’s</b> Course at the{" "}
                                <b>Institute of Chartered Accountancy of India(ICAI)</b>
                            </div>
                        </div>
                        <div className="row m-0 d-flex flex-row-reverse">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.icsi.edu/home/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about12.png' className="w-100" alt='AboutImage12'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the <b>Company Secretary’s</b> Course at the Institute
                                of Company Secretary of India(ICSI)
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href=" https://courses.varunmalhotra.co.in/learn" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about13.png' className="w-100" alt='AboutImage13'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the <b>Financial Literacy Intensive Programme(FLIP)</b>{" "}
                                by <b>EIFS</b>
                            </div>
                        </div>
                        <div className="row m-0 d-flex flex-row-reverse">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://courses.varunmalhotra.co.in/learn" target="_blank" rel="noreferrer" >
                                    <img loading="lazy" src='/assets/about/about14.png' className="w-100" alt='AboutImage14'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied the <b>Financial Literacy Awareness Programme(FLAP)</b>{" "}
                                by <b>EIFS</b>
                            </div>
                        </div>
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.jainuniversity.ac.in/?utm_source=google%20my%20business" target="_blank" rel="noreferrer" >
                                    <img loading="lazy" src='/assets/about/about21.png' className="w-100" alt='AboutImage15'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied <b>M.Com</b> with a specialization in Accounting and
                                Finance at <b>Jain University</b>
                            </div>
                        </div>
                        <div className={`row m-0 d-flex flex-row-reverse ${styles.lastAbout}`}>
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.sxccal.edu/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about16.jpg' className="w-100" alt='AboutImage16'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied <b>B.Com(Honors)</b> with a specialization in Accounting
                                and Finance at <b>St. Xavier’s College Kolkata</b>
                            </div>
                        </div>
                    </div>



                    <div className={`text-center ${styles.about_labels}`}>
                        <picture>
                            <source media="(min-width:465px)" srcSet="/assets/labels/5.png"></source>
                            <img loading="lazy" src="/assets/labels/5.png" alt='About5'></img>
                        </picture>
                    </div>


                    <div className={styles.aboutSection} data-aos="fade-right" data-aos-delay="100">
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="http://ignou.ac.in/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about17.png' className="w-100" alt='AboutImage17'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied <b>Master’s in Psychology</b> at the{" "}
                                <b>Indira Gandhi National Open University(IGNOU)</b>
                            </div>
                        </div>
                        <div className={`row m-0 d-flex flex-row-reverse ${styles.lastAbout}`}>
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="http://ignou.ac.in/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about17.png' className="w-100" alt='AboutImage172'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied <b>Master’s in Philosophy</b> at the{" "}
                                <b>Indira Gandhi National Open University(IGNOU)</b>
                            </div>
                        </div>
                    </div>



                    <div className={`text-center ${styles.about_labels}`}>
                        <picture>
                            <source media="(min-width:465px)" srcSet="/assets/labels/6.png"></source>
                            <img loading="lazy" src="/assets/labels/6.png" alt='About6'></img>
                        </picture>
                    </div>


                    <div className={styles.aboutSection} data-aos="fade-up" data-aos-delay="100">
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.sxcsccu.edu.in/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about18.png' className="w-100" alt='AboutImage18'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied and appeared for <b>ICSE</b> and <b>ISC</b> examination
                                at <b>St. Xavier’s Collegiate School</b>
                            </div>
                        </div>
                        <div className={`row m-0 d-flex flex-row-reverse ${styles.lastAbout}`}>
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="https://www.facebook.com/SMSKolkata" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about19.png' className="w-100" alt='AboutImage19'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                                Studied primary school <b>foundation subjects</b> at{" "}
                                <b>St. Mary’s High School</b>
                            </div>
                        </div>
                    </div>

                    <div className={`text-center ${styles.about_labels}`}>
                        <picture>
                            <source media="(min-width:465px)" srcSet="/assets/labels/7.png"></source>
                            <img loading="lazy" src="/assets/labels/7.png" alt='About7'></img>
                        </picture>
                    </div>

                    <div className={`${styles.aboutSection} mt-2`} data-aos="fade-up" data-aos-delay="100">
                        <div className="row m-0">
                            <div className={`col-3 col-sm-3 p-0 ${styles.aboutpageContainer}`}>
                                <a href="#" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/about/about20.jpg' className="w-100" alt='AboutImage20'></img>
                                </a>
                            </div>
                            <div className={`col-9 col-sm-9 m-auto ${styles.bluetext} ${styles.fw600} ${styles.mfo12}`}>
                            Successfully mentored by Mr. <b>Geoff Futch, PhD Scholar and Doctoral Fellow</b> in <b>Biomechanics and Exercise Physiology </b>
                            at Springfield College, <b>M.Ed in Exercise Science</b> at Auburn University, <b>B.S in Kinesiology</b> at the University of Louisiana at Lafayette, Founder of the <b>FitPro Analytics,</b> Co-Founder of the <b>Human Performance Analytics, </b>Course Facilitator at University of Western States, Former Adjunct Professor at St. Edward's Univerity and Graduate Teaching Assistant at The University of Texas at Austin.
                            </div>
                        </div>
                        
                        <div className={`${styles.aboutSection} mt-5`} data-aos="fade-up" data-aos-delay="100">
                            <div className={`${styles.fo18} ${styles.mfo12} homeBottom`}>
                                <b>Disclaimer -</b>Kindly note that having studied a module
                                offered by an organisation or an institution doesn’t necessarily
                                signify that Alan Baptist is certified in the course corresponding
                                to the modules studied.
                            </div>
                        </div>
                    </div>


                </div>
            </section>

            <Footer />
        </div>
    )
}

export default About;

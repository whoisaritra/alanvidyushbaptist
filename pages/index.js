import Head from 'next/head';
import AboutSection from '../components/sections/AboutSection';
import HeaderSection from '../components/sections/HeaderSection';
import AssociationsSection from '../components/sections/AssociationsSection';
import Footer from '../components/layout/Footer';

export default function Home() {
  return (
    <div className="mt-0">
      <Head>
        <meta property="og:image" content={"https://assets.alanvidyushbaptist.com/logo.jpg"} />
        <title>Alan Vidyush Baptist</title>
      </Head>
      <HeaderSection />
      <AboutSection />
      <AssociationsSection />
      <Footer />
    </div>
  )
}

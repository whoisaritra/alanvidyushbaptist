import { useEffect, useState } from "react";
import ServiceCard from "../../components/cards/ServiceCard";
import urlSet from "../../utils/urls";
import Image from 'next/image';
import Link from 'next/link';

// import ServicesBanner from "../../img/banners/services_banner.png";
import Footer from "../../components/layout/Footer";
import Loader from "../../components/loaders/Percentage";
import Head from "next/head";

const Mainservices = () => {
  const [services, setServices] = useState([]);
  const [isLoading, setLoading] = useState(false);
  useEffect(() => {
    function getMainServices() {
      setLoading(true)
      var request = new XMLHttpRequest();
      request.open(
        urlSet.getMainServiceApi.method,
        urlSet.getMainServiceApi.url + "?level=0",
        true
      );
      request.setRequestHeader("Content-Type", "application/json");
      request.send();
      request.onload = function () {
        var data = JSON.parse(this.response);
        setServices(data);
        setLoading(false);
      };
    }

    getMainServices();
  }, []);

  return (
    <div className="mt-0">
      <Head>
        <title>Services - Alan Vidyush Baptist</title>
      </Head>
      <section className="dynamicPageSection">
        <section>
          <div className="text-center">
            <img src='/assets/banners/services_banner.png' className="w-100" alt="ServicesBanner"></img>
          </div>
        </section>

        <section className="services_section homeBottom">
          <div className="row m-0">
            {isLoading && (
              <Loader />
            )}
            {services && services.map((service, index) => <ServiceCard service={service} key={index}/>)}
          </div>
        </section>
      </section>

      <Footer />
    </div>
  );
};

export default Mainservices;

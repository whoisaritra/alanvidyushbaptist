import { useEffect, useState } from "react";
import { useRouter } from 'next/router';
import urlSet from "../../utils/urls";
import ServiceCard from "../../components/cards/ServiceCard";
// import ServicesBanner from "../../../img/banners/services_banner.png";
import Footer from "../../components/layout/Footer";
import Loader from "../../components/loaders/Percentage";
import Head from "next/head";

const Services = () => {
  const [services, setServices] = useState([]);
  const [isLoading, setLoading] = useState(false);
  const router = useRouter();
  const params = router.query;
  // console.log(params);
  useEffect(() => {
    var mainServicesData;

    function getServices() {
      var request = new XMLHttpRequest();
      setLoading(true)
      request.open(
        urlSet.viewServicesApi.method,
        urlSet.viewServicesApi.url + params.serviceId.replaceAll("_", " "),
        true
      );
      request.setRequestHeader("Content-Type", "application/json");
      request.send();
      request.onload = function () {
        var data = JSON.parse(this.response);
        // console.log(data);
        mainServicesData = data["subservices"];
        setServices(mainServicesData);
        setLoading(false)
      };
    }
    if(params.serviceId){
      getServices();
    }
  }, [params]);

  return (
    <div className="mt-0 mb-5">
      <Head>
        <title>Services</title>
      </Head>
      <section className="dynamicPageSection">
        <section>
          <div className="text-center">
            <img src='/assets/banners/services_banner.png' className="w-100" alt="ServicesBanner"></img>
          </div>
        </section>

        <section className="services_section homeBottom">
          <div className="row m-0">
            {isLoading && (
              <Loader />
            )}
            { !isLoading && services.length == 0 && (
              <div className="page-message">
                Thank you for visiting this page! We are glad to see you&apos;re interested in optimizing this aspect of your wellness. We are currently optimizing our plans and programmes on this page and the details shall be available soon.
              </div>
            )}
            {services && services.length > 0 &&services.map((service, index) => <ServiceCard service={service} key={index} />)}
          </div>
        </section>
      </section>

      <Footer />
    </div>
  );
};

export default Services;

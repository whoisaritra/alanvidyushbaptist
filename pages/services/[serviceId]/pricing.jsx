import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect, useState } from "react";
import urlSet from "../../../utils/urls";
import axios from "axios";
import Slider from "react-slick";
import OfferCard from "../../../components/cards/OfferCard";

import Loader from "../../../components/loaders/Percentage";
import Footer from '../../../components/layout/Footer';
import Head from 'next/head';

const SampleNextArrow = (props) => {
  const { onClick } = props;
  return (
    <i
      className="fas fa-long-arrow-alt-right"
      onClick={onClick}
      style={{
        bottom: "-30px",
        position: "absolute",
        right: "47%",
        fontSize: "26px",
      }}
    />
  );
};

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  console.log(className);
  return (
    <div
      className="fas fa-long-arrow-alt-left"
      style={{
        ...style,
        bottom: "-30px",
        position: "absolute",
        right: "53%",
        fontSize: "26px",
      }}
    />
  );
}

const settings = {
  infinite: true,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 2,
  arrows: true,
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />,
  responsive: [
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        arrows: false,
      },
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
      },
    },
  ],
};

const testimonialsSettings = {
  infinite: true,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 2,
  autoplay: true,
  // arrows: true,
  // nextArrow: <SampleNextArrow />,
  // prevArrow: <SamplePrevArrow />,
  responsive: [
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        arrows: false,
      },
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
      },
    },
  ],
};

const Pricing = () => {
  const [service, setService] = useState(null);
  const [isLoading, setLoading] = useState(false)
  const router = useRouter();
  const params = router.query;
  useEffect(() => {
    const getPlans = async () => {
      try {
        setLoading(true)
        const res = await axios.get(
          urlSet.viewServicesApi.url + params.serviceId.replaceAll("_", " ")
        );
        setService(res.data);
        setLoading(false)
        // console.log(res.data);
      } catch (err) {
        console.log(err);
        setLoading(false)
      }
    };
    getPlans();
  }, [params.serviceId]);
  return (
    <div className='page'>
      <Head>
        <title>Pricing - {service && service.service}</title>
      </Head>
      {isLoading && (
        <Loader />
      )}

      <section>
        <div className="text-center">
          <img src='/assets/banners/pricing_banner.png' className="w-100" alt="PricingBanner"></img>
        </div>
      </section>



      <section className="pricing-section pt-4">
        <div className="pricing-section-container">
          <h2 className="text-center pt-3 pe-1 ps-1">
            {service && "We've Got Plans for " + service.service}
          </h2>
          <hr />
          {service && service.offers.length <= 0 && (
            <h4 className="mb-5 mt-4 text-center">No Offers Currently</h4>
          )}
          <div className='pricingSectionBox'>
            <Slider {...settings}>
              {service &&
                service.offers.map((offer, index) => {
                  return (
                    <OfferCard dark={index % 2 === 0} offer={offer} key={index} />
                  );
                })}
            </Slider>
          </div>
        </div>
      </section>



      <div className="text-center p-4 mt-sm-3 testimonials-heading pb-2">Testimonials</div>
      <hr style={{ borderBottom: "3px solid #FFE972", maxWidth: "100px", margin: "5px auto 40px auto" }} />

      <div style={{ overflow: "hidden" }} className="homeBottom">
        <Slider {...testimonialsSettings}>

          <div className="p-4">
            <img src="/assets/testimonials/1.png" className="w-100 copyright_img no-outline" alt={"image_1"} />
          </div>
          <div className="p-4">
            <img src="/assets/testimonials/2.png" className="w-100 copyright_img no-outline" alt={"image_2"} />
          </div>
          <div className="p-4">
            <img src="/assets/testimonials/3.png" className="w-100 copyright_img no-outline" alt={"image_3"} />
          </div>
          <div className="p-4">
            <img src="/assets/testimonials/4.png" className="w-100 copyright_img no-outline" alt={"image_4"} />
          </div>

        </Slider>
      </div>

      <Footer />
    </div>
  );
};

export default Pricing;

import { useState, useEffect } from "react";
import { useRouter } from "next/router";
import { rootCategories, blogCatandSub } from "../../utils/blogCategories";
import blogText from "../../utils/blogText.json";

import Footer from "../../components/layout/Footer";
import BlogsCategoryBar from "../../components/sections/BlogsCategoryBar";
import BlogCard from "../../components/cards/BlogCard";

import axios from "../../utils/axios";
import urlSet from "../../utils/urls";

import styles from "../page.module.css";
import SimpleLoader from "../../components/loaders/Simple";
import Head from "next/head";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames";

const Blogs = () => {
  const limit = 6;
  const router = useRouter();
  const [page, setPage] = useState(0);
  const [categories, setCategories] = useState([]);
  const [blogs, setBlogs] = useState([]);
  const [loading, setLoading] = useState(true);
  const params = router.query;

  const isSubCategory = params.category;

  const blogsRoot = params.partner != null || params.search != null;
  const getNextBlogs = async () => {
    try {
      let url =
        urlSet.get_blogApi.url +
        "?minimal=true&index=" +
        page * limit +
        "&limit=" +
        limit;
      setLoading(true);
      const res = await axios.get(encodeURI(url));
      if (page === 0) {
        setBlogs(res.data);
      } else {
        setBlogs([...blogs, ...res.data]);
      }

      setPage(page + 1);
      setLoading(false);
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };

  useEffect(() => {
    const fetchBlogs = async () => {
      try {
        let url = urlSet.get_blogApi.url + "?minimal=true&limit=6";
        if (params.partner) {
          url = urlSet.get_AuthorblogApi.url + params.partner;
        } else if (params.search) url = url + "&searchQuery=" + params.search;
        else if (params.category && params.subcategory) {
          url =
            url +
            `&category=${params.category}&subcategory=${params.subcategory}`;
        } else if (params.category) {
          url = url + `&category=${params.category}`;
        }

        setLoading(true);
        const res = await axios.get(encodeURI(url));
        setBlogs(res.data);
        setPage(1);
        setLoading(false);
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    };

    if (params.category) {
      setCategories(blogCatandSub[params.category]);
    } else {
      setCategories(rootCategories);
    }
    fetchBlogs();
  }, [params, params.partner, params.search]);

  const categoryHandler = (id) => {
    if (isSubCategory) {
      router.push(`/blogs?category=${params.category}&subcategory=${id}`);
    } else {
      router.push(`/blogs?category=${id}`);
    }
  };

  const categoryResetHandler = () => {
    router.push(`/blogs`);
  };

  const blogTextHandler = () => {
    if (params.category && params.subcategory) {
      const y = blogText.filter(
        (b) =>
          b.category === params.category && b.subcategory === params.subcategory
      );
      if (y.length === 1) {
        return y[0].text;
      } else {
        const x = blogText.filter((b) => b.category === params.category);
        return x[0].text;
      }
    } else if (params.category) {
      const y = blogText.filter((b) => b.category === params.category);
      return y[0].text;
    } else {
      return "Read On";
    }
  };

  return (
    <div>
      <Head>
        <title>Blogs - Alan Vidyush Baptist</title>
      </Head>
      <div className="dynamicPageSection">
        <section className="allBlogsSection">
          {!params.partner && !params.search && (
            <>
              {" "}
              <div
                className={`text-center ${styles.fo40} ${styles.fw800} ${styles.mfo32}`}
                id="blogPage_heading"
              >
                {params.category != null && params.subcategory != null
                  ? params.subcategory
                  : params.category != null
                  ? params.category
                  : "Blogs"}
              </div>
              <div className={`text-center ${styles.fo20} ${styles.fw600}`}>
                <img
                  src={
                    params.category && params.subcategory
                      ? encodeURI(
                          `https://assets.alanvidyushbaptist.com/icons/${params.subcategory} Yellow.png`
                        )
                      : params.category
                      ? encodeURI(
                          `https://assets.alanvidyushbaptist.com/icons/${params.category} Yellow.png`
                        )
                      : `https://assets.alanvidyushbaptist.com/icons/Blog_Yellow.png`
                  }
                  alt={"icon"}
                  height="60px"
                  width="60px"
                  loading="lazy"
                />
              </div>
              <div
                className={`text-center ${styles.fo15} ${styles.txtco} ${styles.mfo13}`}
              >
                {blogTextHandler()}
              </div>
            </>
          )}

          {!params.partners && !params.search && (
            <div
              className={`navbar navbar-expand-sm mt-4 p-0 ${styles.bgDark} ${styles.blogCategoryNavbar}`}
            >
              <div
                className={`d-block d-sm-none text-white py-3 px-4 ${styles.fw600} ${styles.fo20}`}
              >
                TOPICS
              </div>
              <button
                className="navbar-toggler collapsed"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#collapsibleBlogCategories"
                aria-expanded="false"
                aria-controls="collapsibleBlogCategories"
              >
                <FontAwesomeIcon
                  icon={faChevronDown}
                  className={classNames(
                    `${styles.fo20} ${styles.fw600} text-white`
                  )}
                />
              </button>
              <BlogsCategoryBar
                params={params}
                categories={categories}
                onClick={categoryHandler}
                resetHandler={categoryResetHandler}
                selected={params.subcategory || params.category}
              />
            </div>
          )}

          {!blogsRoot && blogs && blogs.length > 0 && (
            <div className={`position-relative ${styles.blogBanner}`}>
              <img
                src={blogs && blogs[0].headerImage[0].image}
                className="w-100"
                alt={blogs && blogs[0].headerImage[0].name}
                loading="lazy"
              />
              <div className={styles.imageOverlay}>
                <div
                  className={`${styles.fo52} ${styles.fw600} text-center ${styles.mfo20}`}
                >
                  {blogs && blogs[0].title}
                </div>
                <div className="text-center mt-3 mt-md-5">
                  <a href={"/blog/" + blogs[0].slug} rel="noreferrer">
                    <button className={`btn ${styles.readMoreButton}`}>
                      READ MORE
                    </button>
                  </a>
                </div>
              </div>
            </div>
          )}

          <div>
            <div
              className={`${styles.textDark} ${styles.fo30} ps-5 pe-5 pb-5 text-center ${styles.fw700} ${styles.mfo22} text-uppercase`}
            >
              {params.partner && "by " + params.partner.replaceAll("_", " ")}
              {params.search && 'Searched results of "' + params.search + '"'}
            </div>

            <div className="row m-0 mt-3" id="displayAllBlogs">
              {blogs.map((blog, index) => (
                <BlogCard blog={blog} key={index} />
              ))}
            </div>

            {loading && <SimpleLoader />}
            {!loading && blogs && blogs.length > 0 && (
              <>
                <div
                  className="text-center mt-5 homeBottom"
                  id="readmorebutton"
                >
                  {limit * page <= blogs.length ? (
                    <button
                      className={`btn ${styles.readMoreButton}`}
                      onClick={getNextBlogs}
                    >
                      READ MORE
                    </button>
                  ) : (
                    <div
                      className={`${styles.fo18} ${styles.fw600} text-center`}
                    >
                      <p>Revisit us again for more!</p>
                    </div>
                  )}
                </div>
              </>
            )}

            {router.query && !loading && blogs && blogs.length <= 0 && (
              <h2 className="text-center">No Blogs Found</h2>
            )}
          </div>
        </section>
      </div>

      <Footer />
    </div>
  );
};

export default Blogs;

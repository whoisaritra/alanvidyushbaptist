import { useState, useEffect } from 'react';
import axios from 'axios';
import styles from './page.module.css';
import urlSet from '../utils/urls';
import BlogCard from '../components/cards/BlogCard';
import Footer from '../components/layout/Footer';
import Head from 'next/head';

const Columnists = () => {
  const limit = 6;
  const [page, setPage] = useState(1);
  const [blogs, setBlogs] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchBlogs = async () => {
      try {
        let url = urlSet.get_blogApi.url + "?limit=6";

        setLoading(true);
        const res = await axios.get(encodeURI(url));
        console.log(res);
        setBlogs(res.data);
        setPage(1);
        setLoading(false);
      } catch (err) {
        console.log(err);
        setLoading(false);
      }
    };

    fetchBlogs();
  }, []);

  const getNextBlogs = async () => {
    try {
      let url =
        urlSet.get_blogApi.url + "?index=" + page * limit + "&limit=" + limit;
      setLoading(true);
      const res = await axios.get(encodeURI(url));
      if (page === 0) {
        setBlogs(res.data);
      } else {
        setBlogs([...blogs, ...res.data]);
      }

      setPage(page + 1);
      setLoading(false);
    } catch (err) {
      console.log(err);
      setLoading(false);
    }
  };
  return (
    <div className='page mt-0'>
      <Head>
        <title>Columnists - Alan Vidyush Baptist</title>
      </Head>
      <section className='websiteSection websiteSectionc'>
        <section>
          <div className="text-center">
            <img loading="lazy" src='/assets/banners/columnists_banner.png' className="w-100" alt="ColumnistsBanner"></img>
          </div>
        </section>
        <div className={`${styles.textDark} ${styles.fo30} ${styles.mfo22} p-3 mt-4 mt-sm-0 p-sm-5 text-center ${styles.fw700}`}>
          THE LATEST FROM OUR COLUMNISTS
        </div>

        <div className="row m-0 mt-3" id="displayAllBlogs">
          {blogs.map((blog, index) => (
            <BlogCard blog={blog} key={index} />
          ))}
        </div>

        {!loading && blogs && blogs.length > 0 && (
          <>
            <div className="text-center mt-5 homeBottom" id="readmorebutton">
              {limit * page <= blogs.length ? (
                <button
                  className="btn website-button bg-dark text-white"
                  onClick={getNextBlogs}
                >
                  READ MORE
                </button>
              ) : (
                <div className="fo-20 fw-600 text-center">
                  <p>Revisit us again for more!</p>
                </div>
              )}
            </div>
          </>
        )}

        {!loading && blogs && blogs.length <= 0 && (
          <h2 className="text-center">No Blogs Found</h2>
        )}
      </section>

      <Footer />
    </div>
  )
}

export default Columnists;

import { useEffect } from 'react';
import AOS from 'aos';
import Head from "next/head";
import Script from 'next/script';

import { config } from '@fortawesome/fontawesome-svg-core'
import '@fortawesome/fontawesome-svg-core/styles.css'
config.autoAddCss = false

import "aos/src/sass/aos.scss";
import 'bootstrap/dist/css/bootstrap.css'
import '../styles/globals.css'

import "slick-carousel/slick/slick.css";               //here
import "slick-carousel/slick/slick-theme.css";

import Navbar from '../components/layout/Navbar';
import Notification from '../components/layout/Notification';

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    AOS.init({
      duration: 2000,
      easing: "ease-out-cubic",
      once: true,
      offset: 50,
    });
    window.addEventListener("popstate", () => console.log("Asd"));
  }, []);
  return(
    <>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta property="og:image" content={"https://assets.alanvidyushbaptist.com/logo.jpg"} />
      </Head>
      <span id="top"></span>
      <Script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous" />
      <Navbar />
      <main>
        <Component {...pageProps} />
      </main> 
      <Notification />
    </>
  ) 
  
}

export default MyApp

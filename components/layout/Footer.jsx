import styles from './Layout.module.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowUp, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faInstagram, faLinkedinIn, faWhatsapp } from '@fortawesome/free-brands-svg-icons';

const Footer = () => {
    const scrollTopHandler = () => {
        document.getElementById("top").scrollIntoView();
      }
    return (
      <footer>
        <div className={`${styles.whatsapp_icon}`}>
            <div className={`text-white ${styles.bgGray}`}>
                Talk to Us <span className={styles.bordeGray}></span>
            </div>
            <a href="https://wa.me/919836143134?text=Greetings%20good%20sir!%0A%0AI%20found%20your%20website%20online%20and%20had%20an%20enquiry%20to%20make." className="position-relative">
                <img src="https://upload.wikimedia.org/wikipedia/commons/5/5e/WhatsApp_icon.png" alt='whatsappicon'/>
                <span className={styles.whatsapp_icon_span}>1</span>
            </a>
        </div>

        <div className={styles.pageContact}>
            <img src="/assets/footer.png" alt="FooterImage"/>
            <div className={styles.bottomFooter}>
                <div className={styles.footerHeading}>CONTACT</div>
                <hr />
                <div className={styles.footerSocial}>
                    <a href="https://www.facebook.com/alanvidyushandrew.baptist" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon icon={faFacebook} />
                    </a>
                    <a href="https://www.instagram.com/alan_baptist/" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon icon={faInstagram} />
                    </a>
                    <a href="https://www.linkedin.com/in/alanvidyushbaptist" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon icon={faLinkedinIn} />
                    </a>
                    <a href="https://alan.optimumwellness@gmail.com" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon icon={faEnvelope} />
                    </a>
                    <a href="https://api.whatsapp.com/send?phone=919836143134&text=Greetings%20good%20sir!%0A%0AI%20found%20your%20website%20online%20and%20had%20an%20enquiry%20to%20make." target="_blank" rel="noreferrer">
                        <FontAwesomeIcon icon={faWhatsapp} />
                    </a>
                </div>
                <div className={styles.credits}>&copy; 2021 Alan Baptist | Website Developed by <span>Pratiti Bera</span></div>
                <div className={styles.credits}>Website Co-Developed by <span>Aritra Bhattacharjee</span></div>
            </div>
            <div className={styles.scrollTopButton} onClick={scrollTopHandler} data-aos="fade-up" data-aos-delay="1000">
                <FontAwesomeIcon icon={faArrowUp} />
            </div>
        </div>
      </footer>
    );
  };
  
  export default Footer;
  
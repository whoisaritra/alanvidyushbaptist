import { useState, useEffect } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import styles from './Layout.module.css';
import useCart from "../../store/store";

import { faShoppingCart } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import Sidebar from './Sidebar';
import CheckoutModal from './CheckoutModal';

const Navbar = () => {
    const router = useRouter();
    const shopCart = useCart((state) => state.cart);
    const showNotification = useCart((state) => state.showNotification);
    const [show, setShow] = useState(false);
    const [checkout, setCheckout] = useState(false);

    const showCheckout = () => {
        if(shopCart.length > 0){
            setCheckout(true);
            
        } else {
            showNotification("No Items In Cart");
        }
    };
    const closeCheckout = () => setCheckout(false);
    
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
     if (show) {
        setShow(!show);
     }
   }, [router.asPath]);

    return(
        <nav className={styles.navbar} id="navbar">
            <Sidebar show={show} handleClose={handleClose}/>
            <CheckoutModal show={checkout} handleClose={closeCheckout} />
            <div className={styles.topBar}>
                <div className={styles.menuIcon} onClick={handleShow}><img src='/assets/icons/menu-icon.svg' alt="Menu Icon" height={36} width={36}/></div>
                <div className={styles.brand}><Link href="/"><span>ALAN BAPTIST</span></Link></div>
                <Link href="/#contact"><button className='btn'>CONSULT</button></Link>
            </div>
            <div className={styles.bottomBar}>
                <div className='link'><Link href='/about'><div><img src='/assets/icons/about.png' alt="about" height={22} width={22}/> ABOUT ALAN</div></Link></div>
                <div className='link'><Link href='/#success'><div><img src='/assets/icons/story.png' alt="about" height={22} width={22}/> SUCCESS STORIES</div></Link></div>
                <div className='link'><Link href='/blogs'><div><img src='/assets/icons/Blog_White.png' alt="about" height={22} width={22}/> BLOGS</div></Link></div>
                <div className='link'><Link href='/services'><div><img src='/assets/icons/services.png' alt="about" height={22} width={22}/> SERVICES</div></Link></div>
                <div className='link'><Link href='/#contact'><div><img src='/assets/icons/contact.png' alt="about" height={22} width={22}/> CONTACT</div></Link></div>
            </div>
            <div id="cart-icon" className={styles.cartIcon} onClick={showCheckout}><FontAwesomeIcon icon={faShoppingCart} /></div>
            <div className={styles.cartCount}>{shopCart.length > 0 && shopCart.length}</div>
        </nav>
    )
}

export default Navbar;
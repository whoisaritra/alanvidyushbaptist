import { useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import useCart from "../../store/store";
import urlSet from '../../utils/urls';
import styles from './Layout.module.css';

const CheckoutModal = ({ show, handleClose }) => {
  const shopCart = useCart((state) => state.cart);
  const totalBill = useCart((state) => state.total);
  const emptyCart = useCart((state) => state.clearCart);
  const couponCode = useCart((state) => state.couponCode);
  const applyCoupon = useCart((state) => state.applyCoupon);
  const discount = useCart((state) => state.discount);
  const removeCoupon = useCart((state) => state.removeCoupon);
  const showNotification = useCart((state) => state.showNotification);

  const getOffers = () => shopCart.map(item => item['_id'])
  const isValid = () => {
    var customer_name = document.getElementById("customer_name").value;
    var customer_mobile = document.getElementById("customer_mobile").value;
    var customer_email = document.getElementById("customer_email").value;
  
    if (customer_name !== "") {
      if (customer_mobile !== "" && isPhoneNumber(customer_mobile) === true) {
        if (customer_email !== "" && validEmail(customer_email) === true) {
          return true;
        } else {
          showNotification("Please enter a valid email ID");
          return false;
        }
      } else {
        showNotification("Please enter a valid phone number");
        return false;
      }
    } else {
      showNotification("Please enter your name");
      return false;
    }
  }

  const initializeRazorpay = () => {
    return new Promise((resolve) => {
      const script = document.createElement("script");
      script.src = "https://checkout.razorpay.com/v1/checkout.js";

      script.onload = () => {
        resolve(true);
      };
      script.onerror = () => {
        resolve(false);
      };

      document.body.appendChild(script);
    });
  };

  const payInCash = () => {
    if (isValid()) {
      const customer_name = document.getElementById("customer_name").value;
      const customer_mobile = document.getElementById("customer_mobile").value;
      const customer_email = document.getElementById("customer_email").value;

      const json = {
        offerIds: getOffers(),
        couponCode: couponCode,
        name: customer_name,
        phone: customer_mobile,
        email: customer_email,
        type: "CASH",
      };
      console.log(json);
      const request = new XMLHttpRequest();
      request.open(urlSet.paymentApi.method, urlSet.paymentApi.url, true);
      request.setRequestHeader("Content-Type", "application/json");
      request.send(JSON.stringify(json));
      request.onload = function () {
        const data = JSON.parse(this.response);
        console.log(data);
        if (data["receipt_id"] !== "") {
          showNotification("Order placed successfully");
          removeCoupon();
          handleClose();
          emptyCart();
        } else {
          showNotification("Payment unsuccessful");
          removeCoupon();
          handleClose();
        }
      };
    }
  };

  const checkout = async () => {
    const res = await initializeRazorpay();

    if (!res) {
      alert("Razorpay SDK Failed to load");
      return;
    }
    if (isValid()) {
      var customer_name = document.getElementById("customer_name").value;
      var customer_mobile = document.getElementById("customer_mobile").value;
      var customer_email = document.getElementById("customer_email").value;


      var json = {
        offerIds: getOffers(),
        couponCode: couponCode,
        name: customer_name,
        phone: customer_mobile,
        email: customer_email,
        type: "ONLINE",
      };
      var request = new XMLHttpRequest();
      request.open(urlSet.paymentApi.method, urlSet.paymentApi.url, true);
      request.setRequestHeader("Content-Type", "application/json");
      request.send(JSON.stringify(json));
      request.onload = function () {
        var data = JSON.parse(this.response);
        console.log(data);
        if (data["receipt_id"] !== "") {
          var options = {
            key: "rzp_test_I4CcsfzCypIJie",
            amount: data["amount"],
            currency: "INR",
            name: "ALAN VIDYUSH BAPTIST",
            description: "Pay For Your Service",
            image:
              "https://alanvidyushbaptist.com/assets/logo/Optimal_Wellness_Logo.png",
            order_id: data["order_id"],
            handler: function (response) {
              payNowResponse(
                response.razorpay_payment_id,
                response.razorpay_order_id,
                response.razorpay_signature,
                data["receipt_id"]
              );
              console.log(response);
            },
            prefill: {
              name: customer_name,
              email: customer_email,
              contact: customer_mobile,
            },
            notes: {
              address: "Give an address here",
            },
            theme: {
              color: "#8b1ef1",
            },
          };
          var rzp1 = new window.Razorpay(options);
          rzp1.on("payment.failed", function (response) {
            showNotification("Sorry! Payment failed due to banks issue");
          });
          rzp1.open();
        } else {
          showNotification("Sorry! Could not process your request");
        }
      };
    }
  }

  const payNowResponse = (
    razorpay_payment_id,
    razorpay_order_id,
    razorpay_signature,
    receipt_id
  ) => {
    var json = {
      razorpay_order_id: razorpay_order_id,
      razorpay_signature: razorpay_signature,
      razorpay_payment_id: razorpay_payment_id,
      receipt_id: receipt_id,
    };
    console.log(json);
    var request = new XMLHttpRequest();
    request.open(
      urlSet.verifyPaymentApi.method,
      urlSet.verifyPaymentApi.url,
      true
    );
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify(json));
    request.onload = function () {
      var data = JSON.parse(this.response);
      console.log(data);
      if (data["message"] === "Order Valid and Successful") {
        showNotification("Payment successful");
        emptyCart();
        removeCoupon();
        handleClose();
      } else {
        showNotification("Payment unsuccessful");
        removeCoupon();
        handleClose();
      }
    };
  };

  const clearCart = () => {
    emptyCart();
    handleClose();
    showNotification("Cart Cleared");
  }

  const applyCouponHandler = async () => {
    const code = document.getElementById('couponCode').value;
    await applyCoupon(code);
  }

  return (
    <Modal show={show} onHide={handleClose}>
      <Modal.Header>
        <Modal.Title>
          <h4 className={`${styles.textDark} ${styles.fw700} ${styles.fo20} mb-0`}>CHECKOUT</h4>
        </Modal.Title>

        <button type="button" className={`${styles.close} ${styles.fw800}`} data-dismiss="modal" onClick={handleClose}>
          &times;
        </button>
      </Modal.Header>

      <Modal.Body>
        <div id="particulars">
          {shopCart && shopCart.map(service => <ParticularCard service={service} key={service['_id']} />)}
        </div>
        <div className={`mt-4 ${styles.emptyCartOption}`}>
          <div onClick={clearCart} data-dismiss="modal">EMPTY CART</div>
        </div>
        <div className={styles.inputData}>
          <input type="text" required id="customer_name" />
          <div className={styles.underline}></div>
          <label>Full Name</label>
        </div>
        <div className={`${styles.inputData} mt-5`}>
          <input type="text" required id="customer_email" />
          <div className={styles.underline}></div>
          <label>Email</label>
        </div>
        <div className={`${styles.inputData} mt-5`}>
          <input type="text" required id="customer_mobile" />
          <div className={styles.underline}></div>
          <label>Contact number</label>
        </div>
        <div className={`row m-0 ${styles.couponsection} mt-3`}>
          <div className="col-6 col-sm-6 ps-0" id="coupon_status">
            <input type="text" placeholder="Coupon code" id="couponCode" value={couponCode}/>
            {discount && (<><div className={`${styles.fo16} ${styles.textDark} ${styles.fw600}`}>{couponCode} Coupon applied</div>
              <div className={`${styles.fo13} ${styles.textDark}`}>Discount: ₹ {discount}</div></>)}
          </div>
          <div className="col-6 col-sm-6 pe-0">
            <button className="btn w-100" onClick={discount ? removeCoupon : applyCouponHandler} id="coupon_button">
              {discount ? "REMOVE COUPON" : "APPLY COUPON"}
            </button>
          </div>
          <div className={`col-12 col-sm-12 text-center ${styles.cash_option}`}>
            <div className="fo-16 fw-700" onClick={payInCash}>
              PAY IN CASH
            </div>
          </div>
        </div>

      </Modal.Body>
      <Modal.Footer className="m-0">
        <button className={`btn ${styles.footerButton1}`} id="totalbill" >₹ {discount? totalBill-discount : totalBill}</button>
        <button className={`btn ${styles.footerButton2}`} onClick={checkout}>PROCEED TO PAYMENT</button>
      </Modal.Footer>
    </Modal>
  );
}

export default CheckoutModal;

const ParticularCard = ({ service }) => {
  return !service.discounted_price ? (<div className="row m-0 mb-2">
    <div className="col-9 col-sm-8 p-0">
      <div className={`${styles.fo13} ${styles.fw600} ${styles.mfo12}`}>{service["offer_name"]} - {service["duration"]}</div>
    </div>
    <div className="col-3 col-sm-4 p-0">
      <div className={`${styles.fo20} ${styles.fw700} ${styles.textRight} ${styles.mfo18}`}>₹ {service["price"]}</div>
    </div>
  </div>) : (<div className="row m-0 mb-2">
    <div className="col-9 col-sm-8 p-0">
      <div className={`${styles.fo13} ${styles.fw600} ${styles.mfo12}`}>{service["offer_name"]} - {service["duration"]}</div>
    </div>
    <div className="col-3 col-sm-4 p-0">
      <div className={`${styles.fo20} ${styles.fw700} ${styles.textRight} ${styles.mfo18}`}>₹  {service["discounted_price"]}</div>
      <div className={`${styles.fo16} ${styles.textRight} ${styles.mfo12}`} style={{ textDecoration: "line-through" }} id="checkout_discount">₹ {service["price"]}</div>
    </div>
  </div>)
}

const isPhoneNumber = (mob) => {
  var numbers = /^\(?([6789][0-9]{9})$/;
  if (mob.match(numbers)) {
    return true;
  } else {
    return false;
  }
};

const validEmail = (mail) => {
  var pattern = /@gmail\.com/i;
  if (pattern.test(mail)) {
    return true;
  } else {
    return false;
  }
};




import { faSearch, faEnvelope } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faInstagram, faLinkedinIn, faWhatsapp } from '@fortawesome/free-brands-svg-icons';

import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Offcanvas } from 'react-bootstrap';
import styles from './Layout.module.css';

const Sidebar = ({ show, handleClose}) => {
    const router = useRouter();
    const [ search, setSearch ] = useState('');
    const onSubmit = (e) => {
        e.preventDefault();
        console.log(e);
        router.push('/blogs?search='+search);
        setSearch('');
        
        return false;
    }

    return (
        <Offcanvas show={show} onHide={handleClose}>
            <Offcanvas.Body as="div">
                <img src="/assets/close-icon.svg" className={styles.closeIcon} onClick={handleClose} alt="CloseIcon"/>
                <section>
                    <div className={styles.sidebarTop}>
                        <div className='link' onClick={handleClose}><Link href='/about'><div><img src='/assets/icons/about.png' alt="about" height={20} width={20}/> ABOUT ALAN</div></Link></div>
                        <div className='link' onClick={handleClose}><Link href='/#success'><div><img src='/assets/icons/story.png' alt="about" height={20} width={20}/> SUCCESS STORIES</div></Link></div>
                        <div className='link' onClick={handleClose}><Link href='/blogs'><div><img src='/assets/icons/Blog_White.png' alt="about" height={20} width={20}/> BLOGS</div></Link></div>
                        <div className='link' onClick={handleClose}><Link href='/services'><div><img src='/assets/icons/services.png' alt="about" height={22} width={22}/> SERVICES</div></Link></div>
                        <div className='link' onClick={handleClose}><Link href='/#contact'><div><img src='/assets/icons/contact.png' alt="about" height={20} width={20}/> CONTACT</div></Link></div>
                    </div>
                    <Link href="/#contact"><button className={`${styles.sidebarButton} btn`} onClick={handleClose}>CONSULT</button></Link>
                </section>

                <hr />

                <section>
                    <div className="link"><Link href="/partners"><div className={styles.SidebarItems}>Partners</div></Link></div>
                    <div className="link"><Link href="/columnists"><div className={styles.SidebarItems}>Columnists</div></Link></div>
                    <div className="link"><Link href="/portfolio"><div className={styles.SidebarItems}>Portfolio</div></Link></div>

                    <form className={styles.Searchbox} onSubmit={onSubmit}>
                        <FontAwesomeIcon icon={faSearch} />
                        <input type="text" name="" placeholder="Search..." onChange={(e) => setSearch(e.target.value)}/>
                    </form>

                    <div className={styles.bottomFooter}>
                        <div className={styles.footerSocial}>
                            <a href="https://www.facebook.com/alanvidyushandrew.baptist" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faFacebook} />
                            </a>
                            <a href="https://www.instagram.com/alan_baptist/" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faInstagram} />
                            </a>
                            <a href="https://www.linkedin.com/in/alanvidyushbaptist" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faLinkedinIn} />
                            </a>
                            <a href="https://alan.optimumwellness@gmail.com" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faEnvelope} />
                            </a>
                            <a href="https://api.whatsapp.com/send?phone=919836143134&text=Greetings%20good%20sir!%0A%0AI%20found%20your%20website%20online%20and%20had%20an%20enquiry%20to%20make." target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faWhatsapp} />
                            </a>
                        </div>
                        <div className={styles.Sidebarcredits}>&copy; 2021 Alan Baptist | Website Developed by <span>Pratiti Bera</span></div>
                        <div className={styles.Sidebarcredits}>Website Co-Developed by <span>Aritra Bhattacharjee</span></div>
                    </div>

                    <div className={`${styles.pageLinks} ${styles.fo11} text-center`}>
                        <Link href="/TermsAndConditions" className="text-white">Terms And Conditions</Link> 
                        <span className="me-2 ms-2">|</span>
                        <Link href="/PrivacyPolicy" className="text-white">Privacy Policy</Link>
                        <span className="me-2 ms-2">|</span>
                        <Link href="/RefundPolicy" className="text-white">Refund Policy</Link>
                    </div>
                </section>
            </Offcanvas.Body>
        </Offcanvas>
    );
}

export default Sidebar;

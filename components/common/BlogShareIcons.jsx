import { faFacebook, faTelegram, faLinkedinIn, faWhatsapp, faTwitter } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './Accordian/Accordian.module.css'

const BlogShareIcons = ({ postTitle, postUrl, postSummary }) => {
    return (
      <div className="col-sm-1">
        <div className="sticky blogShareIcons" id="sticky">
          <div className={`d-block d-sm-none text-center ${styles.fo20} ${styles.fw700} mt-5`}>
            Share this inspiring story!
          </div>
          <hr className="shareBorder d-block d-sm-none" />
          <div className="row d-flex d-sm-block mt-5 mt-sm-0" id="shareBlog">
            <div className="col-1 d-block d-sm-none"></div>
            <div className="col-2 text-center mb-4">
              <div className={styles.blogShareDiv}>
                <a href={`https://www.facebook.com/sharer.php?u=${postUrl}`} target="_blank" rel="noreferrer">
                  <FontAwesomeIcon icon={faFacebook} />
                </a>
              </div>
            </div>
            <div className="col-2 text-center mb-4">
              <div className={styles.blogShareDiv}>
                <a href={`https://telegram.me/share/url?url=${postUrl}`} target="_blank" rel="noreferrer">
                  <FontAwesomeIcon icon={faTelegram} />
                </a>
              </div>
            </div>
            <div className="col-2 text-center mb-4">
              <div className={styles.blogShareDiv}>
                <a href={`https://www.linkedin.com/shareArticle?mini=true&url=${postUrl}/&source=${postUrl}`} target="_blank" rel="noreferrer">
                  <FontAwesomeIcon icon={faLinkedinIn} />
                </a>
              </div>
            </div>
            <div className="col-2 text-center mb-4">
              <div className={styles.blogShareDiv}>
                <a href={`https://twitter.com/intent/tweet?text=${postSummary} ${postUrl}`} target="_blank" rel="noreferrer">
                  <FontAwesomeIcon icon={faTwitter} />
                </a>
              </div>
            </div>
            <div className="col-2 text-center mb-4">
              <div className={styles.blogShareDiv}>
                <a href={`https://api.whatsapp.com/send?text=${postUrl} - ${postSummary}`} target="_blank" rel="noreferrer">
                  <FontAwesomeIcon icon={faWhatsapp} />
                </a>
              </div>
            </div>
            <div className="col-1 d-block d-sm-none"></div>
          </div>
        </div>
      </div>
    );
  };
  
  export default BlogShareIcons;
  
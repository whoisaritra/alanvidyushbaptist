import { useState } from "react";
import styles from "./Accordian.module.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";

const Accordian = ({ title, children }) => {
  const [isActive, setIsActive] = useState(false);
  const closeHandler = () => setIsActive(false)
  return (
    <div className={styles.accordian}>
      <div className={styles.accordianTitle} onClick={() => setIsActive(!isActive)}>
        <div className={`row m-0 p-2 ${styles.cursorPointer}`}>
          <div className={`col-10 col-sm-10 p-0 ${styles.fo18} ${styles.fw700} ${styles.mfo22} ps-xs-0 ps-lg-0`}>
            {title}
          </div>
          <div className="col-2 col-sm-2 text-center p-0 m-auto" style={{ transition: 'transform .5s', transform: isActive ? 'rotate(180deg)' : 'rotate(0deg)'}}>
            <FontAwesomeIcon icon={faChevronDown} className={`${styles.bco} ${styles.fo24}`} />
          </div>
        </div>
        <hr className={styles.horizontalBar} />
      </div>
      <div className={styles.accordionContent} aria-expanded={isActive} onClick={closeHandler}>
        {children}
      </div>
    </div>
  );
};

export default Accordian;

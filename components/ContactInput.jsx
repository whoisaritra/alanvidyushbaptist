import { useState, useEffect, useRef } from 'react';
import codes from 'country-calling-code';
import styles from './Contact.module.css'


const ContactInput = ({ placeholder = "Select Country", onChange }) => {
  const dropdownRef = useRef(null);
  const flagRef = useRef(null);
  const [show, setShow] = useState(false);
  const [selected, setSelected] = useState({ country: 'India', countryCodes: ['91'], isoCode2: 'IN', isoCode3: 'IND' })
  const [countries, setCountries] = useState(codes)

  const countryChangeHandler = (e) => {
    setCountries(codes.filter(m => m.country.toLowerCase().includes(e.target.value.toLowerCase()) || m.countryCodes[0].toString().includes(e.target.value.toLowerCase())))
  }

  const countrySelectHandler = (c) => {
    setSelected(c);
    setShow(false);
  }

  const countryClickHandler = () => {
    setShow(true);
  }

  useEffect(() => {
    if (show) {
      document.addEventListener('click', (e) => {
        if (!dropdownRef.current.contains(e.target) && !flagRef.current.contains(e.target)) {
          setShow(false);
        }
      })
    }
  })

  const changeHandler = (e) => {
    console.log(selected.countryCodes[0])
    onChange({ number: selected.countryCodes[0].toString() + e.target.value, country: selected.isoCode2 })
  }

  return (
    <div className={`${styles.contactComponent} contactInput my-2`}>
      <div className={styles.container}>
        <div ref={flagRef}>
          <span onClick={countryClickHandler}>{selected && (<img
            alt={selected.country}
            src={`http://purecatamphetamine.github.io/country-flag-icons/3x2/${selected.isoCode2.toUpperCase()}.svg`} width="28px" />)}
          </span>
          <span className='coucode' onClick={countryClickHandler}>
            +{selected.countryCodes[0] || '91'}
          </span>
        </div>

        <input type="number" onChange={changeHandler} autoComplete="off" className={styles.contactInput2}/>
      </div>

      <div ref={dropdownRef} className={`${styles.dropdown} ${show && styles.show}`}>
        <input onChange={countryChangeHandler} placeholder="Search" />
        <div className={styles.dropdownContent}>
          {countries.map(c => {
            if (c.country === 'Netherlands Antilles') return;
            return (<div onClick={() => countrySelectHandler(c)} key={c.isoCode3}>
              <img
                alt={c.country}
                src={`http://purecatamphetamine.github.io/country-flag-icons/3x2/${c.isoCode2}.svg`} width="24px" />
              <span className='gray' style={{ marginRight: '4px' }}>+ {c.countryCodes[0]}</span>
              <span>{c.country}</span>
            </div>)
          })}
        </div>
      </div>
    </div >
  )
}

export default ContactInput;

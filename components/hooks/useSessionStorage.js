import { useState, useEffect } from "react";

const useSessionStorage = (name) => {
  const [value, setValue] = useState([])

  useEffect(() => {
    if(sessionStorage.getItem(name)){
        setValue(JSON.parse(sessionStorage.getItem(name)))
    } else {
        setValue([])
    }
  }, [])

  return value
}

export default useSessionStorage
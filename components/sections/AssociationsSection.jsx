import axios from 'axios';
import { useEffect, useState } from 'react';
import urlSet from '../../utils/urls';

import styles from './Sections.module.css';
import HomeBlogSection from './HomeBlogsSection';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebook, faInstagram, faLinkedinIn, faPinterest, faTelegram, faTwitter, faYoutube, faWhatsapp } from '@fortawesome/free-brands-svg-icons';
import { faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';
import ContactSection from './ContactSection';

const AssociationsSection = () => {
    const [blogs, setBlogs] = useState([]);

    useEffect(() => {
        const fetchBlogs = async () => {
            try {
                let url = urlSet.get_blogApi.url + "?limit=5";
                const res = await axios.get(encodeURI(url));
                // console.log(res);
                setBlogs(res.data);
            } catch (err) {
                console.log(err);
            }
        };

        fetchBlogs();
    }, []);

    return (
        <section className={`${styles.associations} overflow-hidden`} id="success">
            <div className={`text-white text-center ${styles.fo40} ${styles.fw600} ${styles.mfo20}`} data-aos="fade-down" data-aos-delay="100">
                MY ASSOCIATIONS
            </div>

            <div className={`container-fluid ${styles.associationsContainer} pt-5`}>
                <div className={`row m-0 ${styles.mb9}`} data-aos="fade-right" data-aos-delay="110">
                    <div className="col-sm-5 m-auto">
                        <div className="p-4 p-sm-0">
                            <a href="#" target="_blank">
                                <img loading="lazy" src='/assets/logos/ow.png' className="w-100" alt='OWImage' />
                            </a>
                        </div>
                    </div>
                    <div className="col-sm-7 ps-sm-3">
                        <div className={`text-white ${styles.fo22} ${styles.fw600} mt-1 mt-sm-0`}>
                            Optimum Wellness
                        </div>
                        <div className={`text-white ${styles.fo14} fst-italics ${styles.fw400} mt-1`}>
                            CEO and Co-Founder
                        </div>
                        <div className={`text-white ${styles.fo13} ${styles.fw400} mt-2`}>
                            Year Established - 2018
                        </div>
                        <br />
                        <div className={`${styles.associationRow} row m-0`}>
                            <a href="https://www.facebook.com/optimumwellnessglobal" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faFacebook} />
                            </a>
                            <a href="https://www.instagram.com/optimum_wellness_official/" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faInstagram} />
                            </a>
                            <a href="https://www.linkedin.com/company/optimum-wellness-official/" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faLinkedinIn} />
                            </a>
                            <a href="https://www.youtube.com/@optimumwellnessofficial" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faYoutube} />
                            </a>
                            <a href="https://t.me/optimum_wellness_stories" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faTelegram} />
                            </a>
                            <a href="https://pin.it/5vp70Tx" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faPinterest} />
                            </a>
                            <a href="https://twitter.com/optimum_world?s=11&t=um1MZ2SS4gT5mAThRJ_Mag" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faTwitter} />
                            </a>
                            <a href="https://wa.me/919836143134?text=Greetings%20good%20sir!%0A%0AI%20found%20your%20website%20online%20and%20had%20an%20enquiry%20to%20make." target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faWhatsapp} />
                            </a>
                        </div>
                        <br />
                        <div className={`text-white ${styles.fo16} mt-2`}>
                            Optimum Wellness is the world’s first and only platform that’s
                            been established to provide research-backed diversified
                            solutions to holistically elevate an individual’s status of
                            wellness.
                            <br />
                            <br />
                            An individual’s wellness is influenced by an increasingly broad
                            scope of factors that most lifestyle coaches have an inadequate
                            understanding of and as a consequence of which most people find
                            themselves struggling to find fulfillment in their life.
                            <br />
                            <br />
                            Owing to Alan’s wealth of experience owing to his exposure in
                            various fields across and the Optimum Wellness brand was founded
                            to offer phenomenal fine-tuned counseling with respect to
                            various aspects of an individual’s life.
                            <br />
                            <br />
                            Optimum Wellness provides integrated lifestyle advice
                            considering all influential factors of a person’s wellness, such
                            as their intellectual wellness, social wellness, financial
                            wellness, dietary wellness, physical wellness,
                            emotional(psychological) wellness, occupational wellness,
                            environmental wellness, spiritual wellness and sexual wellness.
                        </div>
                    </div>
                </div>


                <div className={`row m-0 ${styles.mb9}`} data-aos="fade-left" data-aos-delay="110">
                    <div className="col-sm-5 m-auto">
                        <div className="p-4 p-sm-0">
                            <a href="#" target="_blank">
                                <img loading="lazy" src='/assets/logos/owa.png' className="w-100" alt='OWAImage' />
                            </a>
                        </div>
                    </div>
                    <div className="col-sm-7 pl-sm-3">
                        <div className={`text-white ${styles.fo22} ${styles.fw600}`}>
                            Optimum Wellness Academy
                        </div>
                        <div className={`text-white ${styles.fo14} fst-italics ${styles.fw400} mt-1`}>
                            President and Co-Founder
                        </div>
                        <div className={`text-white ${styles.fo13} ${styles.fw400} mt-2`}>
                            Year Established - 2018
                        </div>
                        <br />
                        <div className={`${styles.associationRow} row m-0`}>
                            <a href="https://www.facebook.com/optimumwellnessacademy/" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faFacebook} />
                            </a>
                            <a href="https://www.instagram.com/optimum_wellness_academy/" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faInstagram} />
                            </a>
                            <a href="https://www.linkedin.com/company/optimum-wellness-academy/" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faLinkedinIn} />
                            </a>
                            <a href="https://www.youtube.com/@optimumwellnessacademy" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faYoutube} />
                            </a>
                            <a href="https://t.me/optimum_wellness_stories" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faTelegram} />
                            </a>
                            <a href="https://pin.it/5vp70Tx" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faPinterest} />
                            </a>
                            <a href="https://twitter.com/academy_optimum?t=rAS41AEg4t3DpDtgSAjLCQ&s=08" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faTwitter} />
                            </a>
                            <a href="https://wa.me/919836143134?text=Greetings%20good%20sir!%0A%0AI%20found%20your%20website%20online%20and%20had%20an%20enquiry%20to%20make." target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faWhatsapp} />
                            </a>
                        </div>
                        <br />
                        <div className={`text-white ${styles.fo16} mt-2`}>
                            Optimum Wellness Academy was co-founded by Alan with a vision to create 
                            1,00,000+ extraordinary wellness, nutrition and fitness professionals around 
                            the globe by 2025. Optimum Wellness Academy is India’s leading evidence-based 
                            wellness, fitness and nutrition academy. They provide evidence-based wellness,
                            fitness and nutrition knowledge to the masses and create extraordinary 
                            budding fitness and nutrition professionals and help them boost their
                            careers in the same. Their scientifically-backed coursework, spear-headed 
                            by Alan, is thoroughly researched and absolutely dynamic as it is constantly 
                            kept upgraded as per the latest developments in wellness, fitness and 
                            nutrition sciences. The courses are taught in such a way using relatable 
                            examples and simplistic analogies that makes it really easy for the students 
                            to comprehend and apply the concepts in real life practical scenarios. 
                            They are the first wellness, nutrition and fitness academy to provide 
                            lifetime free education support to its students and aid them in their 
                            career growth.
                            <br />
                            <br />
                            Hundreds of successful careers of several students as fitness and nutrition 
                            coaches have launched under Alan’s direct one-on-one guidance and meticulous 
                            mentorship. Numerous students of varying age groups and demographics, ranging 
                            from college going kids to housewives with an incredible passion to restart 
                            their potentially glorious careers, have thoroughly learnt applicable 
                            concepts in fitness and nutrition, despite having totally no background 
                            in science merely due to Alan's ability to deliver well-structured lectures 
                            that cater to even the most basic entry-level scholars.
                        </div>
                    </div>
                </div>

                <div className={`row m-0 ${styles.mb9}`} data-aos="fade-right" data-aos-delay="110">
                    <div className="col-sm-5 m-auto">
                        <div className="p-4 p-sm-0">
                            <a href="#" target="_blank">
                                <img loading="lazy" src='https://assets.alanvidyushbaptist.com/img/logos/zest.png' className="w-100" alt='ZESTImage' />
                            </a>
                        </div>
                    </div>
                    <div className="col-sm-7 pl-sm-3">
                        <div className={`text-white ${styles.fo22} ${styles.fw600}`}>Zest Fitness Studio</div>
                        <div className={`text-white ${styles.fo14} fst-italics ${styles.fw400} mt-1`}>
                            Chief Technology Officer and Head Online Nutritionist
                        </div>
                        <div className={`text-white ${styles.fo13} ${styles.fw400} mt-2`}>
                            Year Established - 2015
                        </div>
                        <br />
                        <div className={`${styles.associationRow} row m-0`}>
                            <a href="https://www.facebook.com/zesttnl" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faFacebook} />
                            </a>
                            <a href="https://www.instagram.com/zesttnl/" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faInstagram} />
                            </a>
                            <a href="https://www.linkedin.com/company/zest-fitness-studio/" target="_blank" rel="noreferrer">
                                <FontAwesomeIcon icon={faLinkedinIn} />
                            </a>
                        </div>
                        <br />
                        <div className={`text-white ${styles.fo16} mt-2`}>
                            Zest Fitness Studio, is arguably the finest and the most elite
                            chain of premium gym brands in Kolkata with the highest number
                            of publicly posted client transformations. They have empowered
                            2500+ people to get fitter and stronger for almost over half a
                            decade! They offer step-by-step Fitness Counseling by
                            formulating the right nutritional diet and training methods in
                            order to achieve one’s fitness goals.
                            <br />
                            <br />
                            They specialize in various training methods such as
                            <br />
                            <br />
                            <ul className={styles.UlTag}>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />
                                    High-Intensity Interval Training (H.I.I.T.)
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />
                                    Bodybuilding
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />CrossFit
                                    training methodologies
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />
                                    Hypertrophy-oriented training
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />Sports
                                    Performance Programming
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />
                                    Flexibility and Mobility drills
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />
                                    Powerlifting
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />Olympic
                                    Lifting
                                </li>
                                <li>
                                    <FontAwesomeIcon icon={faLongArrowAltRight} className='me-2' />
                                    Calisthenics and all other popular aspects of fitness.
                                </li>
                            </ul>
                            <br />
                            Zest Fitness Studio was founded not only with the grand vision
                            of creating Kolkata the fittest city in the nation, but also
                            with mission of educating their members regarding the scientific
                            fundamentals behind fitness and exercise topics, a process that
                            inspired the creation of rising stellar fitness professionals
                            across the city with highly successful and robust careers that
                            can easily be traced by to their humble roots at Zest. The
                            existence of Zest Fitness Studio thereby has undoubtedly
                            positively improved Kolkata's available quality of fitness
                            coaching!
                        </div>
                    </div>
                </div>
            </div>

            <HomeBlogSection blogs={blogs} />

            <ContactSection />
        </section>
    )
}

export default AssociationsSection;

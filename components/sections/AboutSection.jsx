import styles from './Sections.module.css';
import Link from 'next/link';

const AboutSection = () => {
    return(
        <section className={`${styles.aboutSection} overflow-hidden`} id="about">
            <div className={`${styles.aboutContainer} ${styles.w70} ${styles.mow90}`}>
                <div className={`${styles.bco} ${styles.fw600} ${styles.fo55} ${styles.mfo25} text-center m-4`} data-aos="fade-left" data-aos-delay="100">
                    ABOUT ALAN
                </div>

                <div className="text-white" data-aos="fade-right" data-aos-delay="130">
                    Alan Baptist, is a highly sought-after lifestyle coach, and the President and the 
                    Co-Founder and Head Nutritionist at the Optimum Wellness Academy, the leading 
                    Evidence-Based wellness, fitness and nutrition educational institution acrosss 
                    the globe. He is also the Managing Director at Optimum Wellness Solutions, a 
                    health-based startup that consults clients to achieve their fitness and health 
                    goals. He is also an evidence-based practitioner and the CTO (Chief Technological 
                    Officer) at Zest Fitness Studios and Optimum Wellness Studios, an elite chain of 
                    gyms founded by his family and operational across Kolkata, India. He also is 
                    India's first OPTIMUM WELLNESS COACH.
                </div>
                <br />
                <div className="text-white" data-aos="fade-right" data-aos-delay="130">
                    Despite having an immensely profound and rich academic and professional background 
                    in the field of finance, accounting, and taxation he decided to foray into the 
                    domain of fitness and nutrition in his early 20s when he felt the need to lose 
                    weight and get fit himself. Conscious about his own body image, Alan began his 
                    longstanding fitness journey as a unwaveringly firm New Year’s resolution in 2017 
                    under the guidance of his brother Vinit Baptist, co-founder and CEO of Zest Fitness 
                    Studio brand of gyms. He lost around 30 kgs of weight across a grueling period of 9 
                    months and reduced his body fat to 9% by following a rigorous fitness routine and 
                    ensuring calorie deficit. He was the modeling subject of several immensely inspiring 
                    fitness photoshoots and magazine features that soon followed post his transformation.
                </div>
                <br />
                <div className="text-white" data-aos="fade-right" data-aos-delay="130">
                    In his quest to get fitter and achieve his desired physique, Alan learned the 
                    science behind fitness and weight loss, nutrition, and dieting. This is when he 
                    realized that fitness and nutrition are vital aspects of an individual’s life that 
                    are currently quite neglected in the modern world, hence considering it his moral 
                    duty to solve this challenge, Alan took it upon himself to generate awareness and 
                    share his knowledge of the same and co-founded OWA(Optimum Wellness Academy) that 
                    provides evidence-based wellness, fitness and nutrition courses.
                </div>
                <br />
                <div className="row m-0">
                    <div className="col-sm-6 p-0" data-aos="fade-right" data-aos-delay="140">
                        <div className="text-white">
                            Alan has mentored over 10000+ individuals across 10 years in achieving 
                            holistic health transformations. He has also actively guided countless 
                            budding fitness trainers to successfully launch their careers in this 
                            ever-expansive field of fitness and nutrition.
                        </div>
                        <br />
                        <div className="text-white">
                            In addition to being an educationalist, finance professional, and fitness 
                            coach, Alan is also a licensed therapist. Furthermore, he has a Master’s 
                            degree in Philosophy as well. He wishes to coach and mentor other 
                            individuals who are struggling with the various dimensions of their wellness. 
                            Being an optimum wellness coach, his life’s objective is to provide 
                            education, enlightenment, and inspiration to people and help them achieve 
                            optimal and holistic wellness in the different dimensions of their life 
                            such as physical, dietary and psychological wellness, financial wellness, 
                            spiritual wellness, sexual wellness, intellectual and occupational wellness 
                            and social wellness.
                        </div>
                    </div>
                    <div className="col-sm-6 p-0 text-center mt-3 mt-sm-0" data-aos="fade-left" data-aos-delay="140">
                        <img loading="lazy" src='/assets/about.jpg' className={`${styles.w80} copyright_img`} alt='AboutImage'/>
                    </div>
                </div>
                <br />
                <div className={`${styles.aboutQuote} fst-italic text-white`} data-aos="fade-up" data-aos-delay="100">
                    <span className={`${styles.bco} ${styles.fw600}`}>"</span>The fool is in many ways the
                    precursor to the expert. An individual who's open to humbly
                    accepting their ignorance stands a greater chance at attaining
                    enlightenment than the self-proclaimed intellectual.
                    <span className={`${styles.bco} ${styles.fw600}`}>"</span>
                </div>
                <br />
                <div className="text-center" data-aos="fade-up" data-aos-delay="100">
                    <Link href="/about"> 
                        <button className={`btn ${styles.pulsating}`}>KNOW MORE</button>
                    </Link>
                </div>
            </div>
        </section>
    )
}

export default AboutSection;

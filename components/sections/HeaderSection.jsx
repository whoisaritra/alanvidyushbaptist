import styles from './Sections.module.css';

const HeaderSection = () => {
    return(
        <div>
            <div className={styles.HeaderContainer}>
                <div className="row m-0">
                    <div className={`col-8 col-sm-6 ${styles.textLeft} text-white`}>
                        <div className={`${styles.fo26} ${styles.mfo15} fst-italic`}>
                            Fitness & Nutrition Coach, Therapist, Educator and Entrepreneur.
                        </div>
                        <div className={`${styles.fo18} ${styles.mfo13} mt-4`}>
                        India’s First <span className={styles.bco}>Optimum Wellness Coach</span>,
                        Alan Baptist is a young <span className={styles.bco}>entrepreneur</span>{" "}
                        with a powerful vision of <span className={styles.bco}>empowering</span>{" "}
                        individuals by positively{" "}
                        <span className={styles.bco}>transforming</span> various aspects of their
                        lifestyles ranging from optimum physical and dietary wellness to
                        psychological, financial, social, spiritual and other critical and
                        popular facets of wellness.
                        </div>

                        <div className={`row d-none d-sm-flex mt-4 mt-sm-4 ${styles.collabs}`}>
                            <div className="col-sm-4">
                                <a href="https://www.gqindia.com/content/how-to-lose-weight-like-this-23-year-old-nutritionist-who-lost-35-kg-in-just-9-months-weight-loss-inspirations" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/logos/gq.png' className="w-100" alt="Brand1" />
                                </a>
                            </div>
                            <div className="col-sm-4">
                                <a href="https://timesofindia.indiatimes.com/life-style/health-fitness/weight-loss/weight-loss-i-was-taunted-by-my-close-friend-for-having-man-boobs/articleshow/67953957.cms" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/logos/toi.png' className="w-100" alt="Brand2" />
                                </a>
                            </div>
                            <div className="col-sm-4">
                                <a href="https://www.rediff.com/getahead/report/fat-to-fit-how-i-lost-30-kg-in-less-than-a-year/20211227.htm" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/logos/rediff.png' className="w-100" alt="Brand3" />
                                </a>
                            </div>
                            <div className="col-sm-4">
                                <a href="https://www.mensxp.com/health/motivation/49170-31-days-of-fitness-alan-lost-30-kgs-got-down-to-9-body-fat-doing-things-the-right-way.html" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/logos/mxp.png' className="w-100" alt="Brand4" />
                                </a>
                            </div>
                            <div className="col-sm-4">
                                <a href="https://m.facebook.com/story.php?story_fbid=4354644617962440&id=100002508336418" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/logos/fe.png' className="w-100" alt="Brand5" />
                                </a>
                            </div>
                            <div className="col-sm-4">
                                <a href="https://m.facebook.com/nfna.education/photos/a.370415893855728/949739359256709/" target="_blank" rel="noreferrer">
                                    <img loading="lazy" src='/assets/logos/ie.png' className="w-100" alt="Brand6" />
                                </a>
                            </div>
                        </div>
                    </div>
                    <div className='col-4 col-sm-6'></div>
                </div>
            </div>


            <div className={`row d-sm-none m-0 ${styles.collabs}`}>
                <div className="col-4">
                    <a href="https://www.gqindia.com/content/how-to-lose-weight-like-this-23-year-old-nutritionist-who-lost-35-kg-in-just-9-months-weight-loss-inspirations" target="_blank" rel="noreferrer">
                        <img loading="lazy" src='/assets/logos/gq.png' className="w-100" alt="Brand1" />
                    </a>
                </div>
                <div className="col-4">
                    <a href="https://timesofindia.indiatimes.com/life-style/health-fitness/weight-loss/weight-loss-i-was-taunted-by-my-close-friend-for-having-man-boobs/articleshow/67953957.cms" target="_blank" rel="noreferrer">
                        <img loading="lazy" src='/assets/logos/toi.png' className="w-100" alt="Brand2" />
                    </a>
                </div>
                <div className="col-4">
                    <a href="https://www.rediff.com/getahead/report/fat-to-fit-how-i-lost-30-kg-in-less-than-a-year/20211227.htm" target="_blank" rel="noreferrer">
                        <img loading="lazy" src='/assets/logos/rediff.png' className="w-100" alt="Brand3" />
                    </a>
                </div>
                <div className="col-4">
                    <a href="https://www.mensxp.com/health/motivation/49170-31-days-of-fitness-alan-lost-30-kgs-got-down-to-9-body-fat-doing-things-the-right-way.html" target="_blank" rel="noreferrer">
                        <img loading="lazy" src='/assets/logos/mxp.png' className="w-100" alt="Brand4" />
                    </a>
                </div>
                <div className="col-4">
                    <a href="https://m.facebook.com/story.php?story_fbid=4354644617962440&id=100002508336418" target="_blank" rel="noreferrer">
                        <img loading="lazy" src='/assets/logos/fe.png' className="w-100" alt="Brand5" />
                    </a>
                </div>
                <div className="col-4">
                    <a href="https://m.facebook.com/nfna.education/photos/a.370415893855728/949739359256709/" target="_blank" rel="noreferrer">
                        <img loading="lazy" src='/assets/logos/ie.png' className="w-100" alt="Brand6" />
                    </a>
                </div>
            </div>
        </div>
    )
}

export default HeaderSection;

import { useState } from 'react'
import Link from "next/link";
import styles from './Sections.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLongArrowAltLeft, faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';

const BlogSection = ({ blogs }) => {
  const [selected, setSelected] = useState(3)
  const prevBlog = () => {
    console.log('prev');
    setSelected(selected - 1 == 0 ? 5 : selected - 1)
  };

  const nextBlog = () => {
    console.log('next');
    setSelected(selected + 1 > 5 ? 1 : selected + 1)
  };

  const selectHandler = (id) => {
    setSelected(parseInt(id))
  }

  return (
    <div className={styles.homeBlogSection} id="blogs">
      <div className={`${styles.fo52} pt-3 ${styles.fw700} ${styles.mfo32} ${styles.pulsating_text} text-center text-white d-flex justify-content-center`}>
        Check out my
        <Link href="/blogs">
          Blogs
        </Link>
      </div>
      <hr />
      <div className="pe-lg-4 ps-lg-4">
        <div className="pe-lg-4 ps-lg-4">
          <div className={`container-fluid pe-0 ps-0 ps-lg-5 pe-lg-5 ${styles.blogSlider}`} id="blogSlider">
            <input type="radio" name="blogSlider" id="s1" value="1" checked={selected === 1}/>
            <input type="radio" name="blogSlider" id="s2" value="2" checked={selected === 2}/>
            <input type="radio" name="blogSlider" id="s3" value="3" checked={selected === 3}/>
            <input type="radio" name="blogSlider" id="s4" value="4" checked={selected === 4}/>
            <input type="radio" name="blogSlider" id="s5" value="5" checked={selected === 5}/>

            {blogs &&
              blogs.length > 0 &&
              blogs.map((blog, index) => {
                if (index < 5)
                  return <BlogCard blog={blog} index={index + 1} key={index} selectHandler={selectHandler}/>;
              })}

            <div className={`text-center ${styles.blogsHandlerIcons}`}>
              <FontAwesomeIcon icon={faLongArrowAltLeft} onClick={prevBlog} className="me-2"/>
              <FontAwesomeIcon icon={faLongArrowAltRight} onClick={nextBlog} className="ms-2"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BlogSection;

const BlogCard = ({ index, blog, selectHandler }) => {
  const clickHandler = () => selectHandler(index)
  return (
    <label htmlFor={`s${index}`} id={`slide${index}`} onClick={clickHandler}> 
      <div className={styles.blogcardimage}>
          { blog['headerImage'].length > 0 && (
            <Link href={"/blog/" + blog["slug"]}>
              <img loading="lazy" src={`${blog["headerImage"][0]["image"]}`} height="100%" width="100%" alt={blog.slug}/>
            </Link>
          )}
      </div>
      <div className={styles.blogBody}>
        <div className={`${styles.fo21} ${styles.fw700} ${styles.mfo12}`}>{blog["title"]}</div>
        <div className={`${styles.fo14} ${styles.blogSummary} mt-3 ${styles.mfo11}`}>
          {blog["summary"]}
        </div>
        <div className="mt-4">
          <Link href={"/blog/" + blog["slug"]}>
            READ MORE
          </Link>
        </div>
      </div>
      <div className={`${styles.blogFooter} ${styles.fo12} ${styles.mfo11}`}>{blog["date"]}</div>
    </label>
  );
};

import styles from './Sections.module.css'

const CoachSection = ({ coaches }) => {
    // console.log(coaches.length);
    const pStyle = coaches.length === 1 ? 'p500' : coaches.length === 2 ? 'p300' : 'p150'; 
    const colDivision = 12 / coaches.length;
    return (
      <div id="coaches">
        <div className={`row m-0 ${pStyle} mt-3`}>
          {coaches &&
            coaches.map((elem, index) => {
              return (
                <div className={`col-${colDivision} col-sm-${colDivision} p-0`} key={index}>
                  <img loading="lazy" src={elem["image"]} alt={`${elem["name"]}`} />
                  <div className={`mt-3 ${styles.fw700} ${styles.mfo11} ${styles.ls9}`}>
                    {elem["designation"] === ""
                      ? `${elem["name"]}`
                      : `${elem["designation"]} : ${elem["name"]}`}
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    );
  };
  
  export default CoachSection;
  

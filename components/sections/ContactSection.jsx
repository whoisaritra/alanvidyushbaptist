import { useState } from 'react';
import ContactInput from '../ContactInput';
import styles from './Sections.module.css'
import useUI from '../../store/store';
import parsePhoneNumber from 'libphonenumber-js'
import axios from 'axios';
import { baseUrl } from '../../utils/urls';

const ContactSection = () => {
  const showNotification = useUI((state) => state.showNotification)
  const [phone, setPhone] = useState(null)
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState({
    name: "",
    email: "",
    message: ""
  });

  const isValidPhoneNumber = (p) => {
    const phoneNumber = parsePhoneNumber(p.number, p.country)
    return phoneNumber.isValid();
  }

  const sendHandler = async (e) => {
    e.preventDefault();
    if (!phone || !isValidPhoneNumber(phone)) {
      alert('Phone Number Not Valid');
      return;
    }
    try {
      setLoading(true);
      await axios.post(
        baseUrl + "/api/v1/feedbacks/",
        {
          ...data,
          phone: phone.number
        }
      );
      showNotification('Message Sent!')
      setLoading(false);
      setPhone('')
      setData({})
      window.scrollTo(0, 0);
    } catch (err) {
      showNotification(err.toString())
      setLoading(false);
      console.log(err);
    }
  };


  return (
    <div id="contact" className='text-center'>
      <div className='contactContainer ms-auto me-auto'>
        <div className={`mb-4 ${styles.bco} ${styles.fo32} ${styles.fw700} ${styles.mfo20}`}>BOOK YOUR FREE CONSULTATION</div>
        <form className="consultancy_form m-auto" onSubmit={sendHandler}>
          <input type="text" placeholder="Enter Name*" value={data.name} onChange={(e) => setData({ ...data, name: e.target.value })} id="connect_name" required autoComplete="off" />
          <input type="text" placeholder="Enter Email*" value={data.email} onChange={(e) => setData({ ...data, email: e.target.value })} id="connect_email" required autoComplete="off" />
          <ContactInput onChange={(value) => setPhone(value)} />
          <textarea placeholder="Enter Message (Optional)" id="connect_message" value={data.message} onChange={(e) => setData({ ...data, message: e.target.value })} autoComplete="off"></textarea>
          <button className={`btn mt-2 ${styles.pulsating}`}>Send</button>
        </form>
      </div>
    </div >
  );
};

export default ContactSection;

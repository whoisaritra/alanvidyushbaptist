import { useRouter } from 'next/router';
import styles from './Sections.module.css';

const BlogsCategoryBar = ({ categories, onClick, resetHandler, selected, params, show }) => {
    const router = useRouter();
    return (
        <div className="collapse d-sm-block w-100" id="collapsibleBlogCategories">
            <div className={`${styles.collapsibleBlogCategories} d-block d-sm-flex`}>
                <div id="category_0" className={`${styles.active} d-none d-sm-flex align-items-center`}>TOPICS</div>
                <div id="category_1" className={router.pathname === '/blogs' ? 'mo-active' : ''} onClick={resetHandler}>
                    <img src={
                            params.category && params.subcategory
                                ? encodeURI(
                                `https://assets.alanvidyushbaptist.com/icons/${params.subcategory} White.png`
                                )
                                : params.category
                                ? encodeURI(
                                    `https://assets.alanvidyushbaptist.com/icons/${params.category} White.png`
                                )
                                : `https://assets.alanvidyushbaptist.com/icons/Blog_White.png`
                        }
                        alt={"All_Blogs"} className="me-2" height="30px" width="30px"/>ALL
                </div>
                {categories && categories.map((category, index) => {
                    return (
                        <div id={"category_" + category.name} key={index} onClick={() => onClick(category.name)} className={category.name === selected ? "active" : ""} type="button" data-bs-toggle="collapse" data-bs-target="#collapsibleBlogCategories" aria-expanded="false" aria-controls="collapsibleBlogCategories" >
                            {category && (
                                <img src={encodeURI(`https://assets.alanvidyushbaptist.com/icons/${category.name} White.png`)} alt={category.name + "_icon"} className="me-2" height="30px" width="30px"/>
                            )}

                            {category.name}
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default BlogsCategoryBar;
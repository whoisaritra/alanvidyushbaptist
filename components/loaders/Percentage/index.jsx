import { useEffect, useState } from 'react'
import styles from '../loaders.module.css';

const Loader = () => {
  const [progress, setProgress] = useState(1)
  // setProgress(1)




  useEffect(() => {
    const timer = setInterval(() => {
      setProgress(progress + 1);
      if (progress >= 100) {
        setProgress(1);
      }
    }, 200)

    return () => clearInterval(timer)
  })


  return (
    <div className={styles.preloader}>
      <div className={styles.progress_outer} id="progress_outer" style={{background: `conic-gradient(#FFE972 ${progress}%,#fff ${progress}%`}}>
        <div className={styles.progress_inner}>
          <span id="counter">{progress} %</span>
        </div>
      </div>
    </div>
  )
}

export default Loader
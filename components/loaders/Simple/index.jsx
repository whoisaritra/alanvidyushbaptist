import styles from '../loaders.module.css';

const SimpleLoader = () => {
    return (
        <div className={styles.simpleLoader}>
            <div className={styles.loader}></div>
        </div>
    )
}

export default SimpleLoader;
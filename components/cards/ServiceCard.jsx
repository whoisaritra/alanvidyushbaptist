import { useRouter } from 'next/router';
import styles from './ServiceCard.module.css'

const ServiceCard = ({ service, index }) => {
  const navigate = useRouter();
  const mainServiceDetails = () => {
    if (service["offers"].length > 0) {
      navigate.push("/services/" + service["service"].replaceAll(" ", "_") + "/pricing");
    } else {
      navigate.push("/services/" + service["service"].replaceAll(" ", "_"));
    }
  };


  return (
    <div className={`${styles.cardContainer} col-xs-12 col-sm-12 col-md-6 col-lg-4`}>
      <div className={styles.cardBackground}>
        <img className={`${styles.backgroundImage} ${styles.firstImage}`} src={service["service_image"][1]} alt="service_image_2"/>
        <img className={`${styles.backgroundImage} ${styles.secondImage}`} src={service["service_image"][0]} alt="service_image_1"/>
      </div>

      <div className={styles.messageBox}>
        <h5>{service["service"]}</h5>
        <p>{service["description"]}</p>
        <div className={`${styles.serviceButton} w-50 ms-auto me-auto mt-3`} id={`service_${service.id}`} onClick={mainServiceDetails}>
          Read More
        </div>
      </div>
    </div>
  );
};

export default ServiceCard;
import Link from 'next/link';
import styles from './ServiceCard.module.css'



const BlogCard = ({ blog }) => {
  const imageName = blog.headerImage.length > 0 ? blog.headerImage[0].image.split('/')[blog.headerImage[0].image.split('/').length - 1] : '';
  // const thumbnailName = 'https://assets.alanvidyushbaptist.com/thumbnails/' + imageName.split('.')[0] + "_thumbnail.jpg";
  return (
    <div className="col-6 col-sm-4 mb-4 p-md-3">
      <Link href={"/blog/" + blog.slug} target="_blank">
        <a>
          <img loading="lazy" src={blog.headerImage[0].image} className={`w-100 ${styles.cursorPointer}`} alt={blog.title} />
        </a>
      </Link>
      <Link href={"/blog/" + blog.slug} target="_blank">
        <a>
          <div className={`${styles.listed_blog_title} ${styles.fo20} ${styles.fw600} text-center ${styles.mfo13} mb-3`}>
            {blog.title}
          </div>
        </a>
      </Link>

      <div className={`${styles.listed_blog_summary} ${styles.fo14} ${styles.fw400} text-center ${styles.mfo11}`}>
        {blog.summary}
      </div>
    </div>
  );
};

export default BlogCard;

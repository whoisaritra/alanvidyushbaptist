import styles from './ServiceCard.module.css'
import useCart from '../../store/store';

const OfferCard = ({ dark, offer }) => {
  const state = useCart((state) => state)
  const addToCart = useCart((state) => state.addToCart)
  const showNotification = useCart((state) => state.showNotification)

  // console.log(state);
  const clickHandler = () => {
    if(offer.in_stock){
      addToCart(offer);
    } else {
      showNotification("No Slots Available");
    }
  }


  return (
    <div className={dark ? "pt-5 pb-5 darkPricing offerCard" : "bg-white pt-5 pb-5 offerCard"}>
      <div className={`${styles.pri_offer_heading} text-center`}>
        {offer.duration}
      </div>

      {offer.recommended == true ? (
        <div className={styles.recommend}>Recommended</div>
      ) : (
        <div className={styles.recommend}></div>
      )}

      {offer.discounted_price && offer.discounted_price > 0 ? (
        <div className={`text-center ${styles.offerPrice}`}>
          <span className={`${styles.fo20} me-2`} style={{ textDecoration: "line-through" }}>
            ₹ {offer.price}
          </span>
          <span className={`${styles.fo26} ${styles.fw600}`}>₹ </span>
          {offer.discounted_price}
        </div>
      ) : (
        <div className={`text-center ${styles.offerPrice}`}>
          <span className={`${styles.fo26} ${styles.fw600}`}>₹ </span>{offer.price}
        </div>
      )}

      <ul className={`${styles.pricingSectionItems} ps-0 mt-3`}>
        {offer.features.map((feature) => {
          return <li key={feature}>{feature}</li>;
        })}
      </ul>
      <div>
        <div className="text-center mt-5">
          <button className={`btn ${styles.offerButton}`} onClick={clickHandler} id="Salary_+_House_Property_Plan"
          // disabled={!offer.in_stock}
          >
            {offer.in_stock ? "CHOOSE PLAN" : "SLOTS FULL"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default OfferCard;

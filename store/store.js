
import create from 'zustand';
import { persist } from 'zustand/middleware';
import axios from 'axios';
import urlSet from '../utils/urls';

const useCart = create(
    persist(
        (set, get) => ({
            cart: [],
            total: 0,
            discount: null,
            notification: null,
            couponCode: null,
            closeNotification:  () => set((state) => ({...state, notification: null})),
            showNotification:  (message) => set((state) => ({...state, notification: message})),
            addToCart: (params) => {
                const check = get().cart.filter(item => {
                  console.log(params['_id'], item['_id']);
                  return params['_id'] === item['_id'];
                });
                console.log(check);
                if(check.length > 0){
                  set((state) => ({
                    notification: 'Already Exists in Cart',
                  }));
                } else {
                  set((state) => ({
                    total: params.discounted_price ? state.total + parseFloat(params.discounted_price) : state.total + parseFloat(params.price),
                    cart: [...state.cart, params],
                    notification: 'Added to Cart',
                  }));
                }
            },
            clearCart: () => set({ cart: [], total: 0 }),
            removeFromCart: (params) =>
                set((state) => ({
                    total: params.discounted_price ? state.total - parseFloat(params.discounted_price) : state.total - parseFloat(params.price),
                    cart: state.cart.filter(
                        (item) => item.id !== params.id
                    ),
                    notification: 'Cart Updated',
                })),
            applyCoupon: async (couponCode) => {
              const offers = get().cart.map(item => item['_id'])
              var json = {
                offerIds: offers,
                couponCode: couponCode,
              };
              var request = new XMLHttpRequest();
              request.open(urlSet.applyCouponApi.method, urlSet.applyCouponApi.url, true);
              request.setRequestHeader("Content-Type", "application/json");
              request.send(JSON.stringify(json));
              request.onload = function () {
                var data = JSON.parse(this.response);
                console.log(data);
                if (data["error"] === "Coupon does not exist") {
                  set({ notification: 'Coupon does not exist.'})
                } else {
                  set({ discount: data["discount"], couponCode: couponCode, notification: 'Coupon Applied.'});
                }
              };
            },
            removeCoupon: () => set({couponCode: null, discount: null, notification: 'Coupon Removed' }),
        }),
        { name: 'cart' }
    )
);
export default useCart;


const applyCoupon = () => {

  };
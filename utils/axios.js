import axios from "axios";

// export const devBaseUrl = 'http://127.0.0.1:8081/';
export const devBaseUrl = "https://itchy-blue-seagull.cyclic.app/";

let headers = {};
if (typeof window !== "undefined") {
  if (localStorage.getItem("accessToken")) {
    headers = {
      authorization: `${localStorage.getItem("accessToken")}`,
    };
  }
}

const instance = axios.create({
  baseURL: devBaseUrl,
  headers: headers,
});

export default instance;

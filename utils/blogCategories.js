export const rootCategories = [
    {
      name: "Fitness",
    },
    {
      name: "Nutrition",
      image: "/assets/icons/Nutrition Concepts White.png",
    },
    {
      name: "Education",
      image: "/assets/icons/Education White.png",
    },
    {
      name: "Psychology",
      image: "/assets/icons/Psychology Concepts White.png",
    },
    {
      name: "Finance",
      image: "/assets/icons/Finance White.png",
    }
  ];
export const blogCatandSub = {
    Fitness: [
      {
        name: "Weight Loss",
        image: "/assets/icons/Weight Loss White.png",
      },
      {
        name: "Muscle Gain",
        image: "/assets/icons/Muscle Gain White.png",
      },
      {
        name: "Body Recomposition",
        image: "/assets/icons/Body Recomposition White.png",
      },
      {
        name: "Sports Performance",
        image: "/assets/icons/Sports Performance White.png",
      },
      {
        name: "Fitness Modelling",
        image: "/assets/icons/Fitness Modelling White.png",
      },
      {
        name: "Fitness Myths",
        image: "/assets/icons/Fitness Myths White.png",
      },
      {
        name: "Postpartum",
        image: "/assets/icons/Postpartum White.png",
      }
    ],
    Nutrition: [
      {
        name: "Weight Loss",
        image: "/assets/icons/Weight Loss White.png",
      },
      {
        name: "Weight Gain",
        image: "/assets/icons/Weight Gain White.png",
      },
      {
        name: "Nutrition Concepts",
        image: "/assets/icons/Nutrition Concepts White.png",
      },
      {
        name: "Recipes",
        image: "/assets/icons/Recipes White.png",
      },
      {
        name: "Nutrition Myths",
        image: "/assets/icons/Nutrition Myths White.png",
      },
    ],
    Education: [
      {
        name: "Career",
        image: "/assets/icons/Career White.png",
      },
      {
        name: "Skills",
        image: "/assets/icons/Skills White.png",
      },
    ],
    Psychology: [
      {
        name: "Sex",
        image: "/assets/icons/Sex White.png",
      },
      {
        name: "Relationships",
        image: "/assets/icons/Relationships White.png",
      },
      {
        name: "Communication",
        image: "/assets/icons/Communication White.png",
      },
      {
        name: "Psychology Concepts",
        image: "/assets/icons/Psychology Concepts White.png",
      },
      {
        name: "Philosophy",
        image: "/assets/icons/Philosophy White.png",
      },
      {
        name: "Spirituality",
        image: "/assets/icons/Spirituality White.png",
      },
    ],
    Finance: [
      {
        name: "Taxation",
        image: "/assets/icons/Taxation White.png",
      },
      {
        name: "Investment",
        image: "/assets/icons/Investment Yellow.png",
      },
      {
        name: "Business",
        image: "/assets/icons/Business White.png",
      },
      {
        name: "Commerce",
        image: "/assets/icons/Commerce White.png",
      },
      {
        name: "Economics",
        image: "/assets/icons/Economics White.png",
      },
    ],
  };


